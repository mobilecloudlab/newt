package pam;

import java.util.ArrayList;

import org.apache.commons.lang.ArrayUtils;

import shared.FloatArrayWritable;
import ut.ee.newt.comm.BSPComm;
import ut.ee.newt.job.Stage;


@SuppressWarnings("serial")
public class PAM {
	
	public static class ScatterPoints extends Stage<PAMState> {
		@Override
		public String execute(BSPComm bsp, PAMState state, int superstep) {
			
			scatterPoints(state, bsp);
			
			return "scatterMedoids";
		}
	}
	
	public static class ScatterMedoids extends Stage<PAMState> {
		@Override
		public String execute(BSPComm bsp, PAMState state, int superstep) {
			receivePoints(state, bsp);
			
			scatterMedoids(bsp, state);
			
			return "dividePoints";
		}
	}
	
	public static class DividePoints extends Stage<PAMState> {
		@Override
		public String execute(BSPComm bsp, PAMState state, int superstep) {
			if (state.it == 0)
				receiveMedoids(bsp, state);
			else
				gatherMedoids(bsp, state);
			
			float distance = PAMMath.distance(state.medoids.getData(), state.oldmedoids.getData());
			System.err.println("Distance " + distance+ "\n");
			if (distance <= 0) {
				//done
				System.err.println("Done in "+state.it+" iterations");
				System.out.println("pam time "+(System.currentTimeMillis()-state.initTime));
				
				return Stage.STAGE_END;
			} else {
				for(int i = 0; i < state.medoids.size(); i++) {
					state.oldmedoids.getData()[i] = state.medoids.getData()[i];
				}
				ArrayList<Float>[] division = PAMMath.dividePoints(state);
				
				sendPoints(bsp, state, division);
				
				return "recalculateMedoids";
			}
			
		}
	}
	
	public static class RecalculateMedoids extends Stage<PAMState> {
		@Override
		public String execute(BSPComm bsp, PAMState state, int superstep) {
			
			receivePoints(bsp, state);
			
			float[] new_medoids = PAMMath.findCenter(state);
			sendMedoids(bsp, state, new_medoids);
			
			state.it++;
			
			long usedKB = (Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory())/1024;
			System.err.println("Memory usage: "+usedKB+" kb. ");
			
			return "dividePoints";
		}
	}
	
	static void scatterPoints(PAMState state, BSPComm comm) {
		int rank = comm.pid();
		float[] medoids = state.medoids.getData();
		
		if(rank == 0){
			float[] allpoints = PAMMath.genPointShape(state.num);
			for (int i = 0; i < state.k; i++) {
				medoids[i*2] = allpoints[i*(allpoints.length/state.k)];
				medoids[i*2+1] = allpoints[i*(allpoints.length/state.k)+1];
			}
			int range = allpoints.length/state.k+((allpoints.length%state.k == 0) ? 0 : 1);
			range += range%2 == 0 ? 0 : 1;
			System.out.println("range: "+range);
			int k = 0;
			for(; k < state.range; k++) {
				int start = k*range;
			    int end = Math.min((k+1)*range, allpoints.length);
			    
			    int length = end-start;
				state.points[k].setData(new float[length]);
				for(int i = 0; i < length; i++) {
					state.points[k].getData()[i] = allpoints[start+i];
				}
			}
			for (; k < state.k; k++) {
				int start = k*range;
			    int end = Math.min((k+1)*range, allpoints.length);
				
			    int length = end-start;
				FloatArrayWritable msg = new FloatArrayWritable(new float[length]);
				for (int j = 0; j < length; j++) {
					msg.getData()[j] = allpoints[start+j];
				}
				msg.tag = k;
				//System.err.println("sending "+msg.size()+" points for "+msg.tag+" to "+(k/state.range)+" ... "+(msg.getData()[0]));
				comm.send(k/state.range, msg);
			}
		}
	}
	
	static void receivePoints(PAMState state, BSPComm comm) {
		if (comm.pid() != 0) {
			while (comm.peek() != null) {
			//for(int k = 0; k < state.end-state.start; k++) {
				FloatArrayWritable recv = (FloatArrayWritable) comm.move();
				//System.err.println("received "+recv.size()+" points for "+recv.tag+" ... "+(recv.getData()[0]));
				state.points[recv.tag-state.start].setData(recv.getData());
				//state.points[k].setData(recv.getData());
			}
		}
	}
	
	static void sendMedoids(BSPComm comm, PAMState state, float[] new_medoids){
		int procs = comm.nprocs();
		
		FloatArrayWritable medoids = new FloatArrayWritable(new_medoids);
		medoids.tag = state.start;
		for (int i = 0; i < procs; i++) {
			comm.send(i, medoids);
		}
	}
							
	static void gatherMedoids(BSPComm comm, PAMState state) {
		while (comm.peek() != null) {
			//int source = comm.peek().source;
			FloatArrayWritable medoid = (FloatArrayWritable) comm.move();
			//System.err.println("got medoids from "+source+" with start "+medoid.tag);
			for(int k = 0; k < medoid.size(); k+=2) {
				state.medoids.getData()[medoid.tag*2+k] = medoid.getData()[k];
				state.medoids.getData()[medoid.tag*2+k+1] = medoid.getData()[k+1];
			}
		}
		//System.err.println("medoids:");
		//for(int k = 0; k < state.medoids.size(); k+=2) {
		//	System.err.print("("+state.medoids.getData()[k]+", "+state.medoids.getData()[k+1]+") ");
		//}
	}
	
	static void scatterMedoids(BSPComm comm, PAMState state) {
		int rank = comm.pid();
		int procs = comm.nprocs();
		if (rank == 0) {
			for (int i = 1; i < procs; i++) {
				comm.send(i, state.medoids);
			}
		}
	}
	
	static void receiveMedoids(BSPComm comm, PAMState state) {
		int rank = comm.pid();
		if(rank != 0) {
			state.medoids.setData(((FloatArrayWritable) comm.move()).getData());
		}
	}

	static void sendPoints(BSPComm comm, PAMState state, ArrayList<Float>[] division) {
		FloatArrayWritable tosend;
		for (int k = 0; k < state.k; k++) {
			if (division[k].size() > 0) {
				tosend = new FloatArrayWritable(new float[division[k].size()]);
				tosend.tag = k;
				for (int j = 0; j < division[k].size(); j++) {
					tosend.getData()[j] = division[k].get(j);
				}
				//System.err.println("sending "+tosend.size()+" points belonging to cluster "+k+" to process "+(k/state.range));
				comm.send(k/state.range, tosend);
			}
		}
	}

	static void receivePoints(BSPComm comm, PAMState state) {
		for (int k = 0; k < state.end-state.start; k++) {
			state.points[k].setData(new float[0]);
		}
		while (comm.peek() != null) {
			//int source = comm.peek().source;
			FloatArrayWritable new_points = (FloatArrayWritable) comm.move();
			int t = new_points.tag-state.start;
			//System.err.println("got points from "+source+" for cluster "+new_points.tag+" ("+(new_points.tag-state.start)+")");
			state.points[t].setData(ArrayUtils.addAll(state.points[t].getData(), new_points.getData()));
		}
	}
}
