package pam;

import java.util.ArrayList;
import java.util.Random;

public class PAMMath {

	static float distance(float a1, float a2, float b1, float b2) {
		double sum = Math.pow((a1-b1), 2) + Math.pow((a2-b2), 2);
		return (float) Math.pow(sum, 0.5);
	}

	static float distance(float[] a, float[] b) {
		float  total = 0;
		for(int i = 0; i < a.length; i += 2)
			total +=  distance(a[i],a[i+1],b[i],b[i+1]);

		return total;
	}
	
	/**
	 * Find the cost of a cluster configuration.
	 * Argument is the potential center of the cluster
	 * 
	 * @param points_ List of points
	 * @param center medoid of the cluster
	 * @return cost of the cluster
	 */
	static float findCost(PAMState state, float[] points, float a, float b) {
		float sum = 0;
		
		//find the sum of all pairwise distances between the center and all the points in the cluster

		for(int i = 0; i < points.length; i+=2)
			sum += distance(points[i], points[i+1], a, b);
		
		//return the cost
		return sum;
	}
	
	
	/**
	 * Find a new center for the cluster
	 * Tries out possible centers and picks the one with minimal configuration cost
	 * @param points2 List of points
	 * @return New medoid
	 */
	static float[] findCenter(PAMState state) {
		
		float cost;
		float bestCost;
		
		float[] bestCenters = new float[(state.end-state.start)*2];
		
		for(int k = 0; k < state.end-state.start; k++) {
			float[] points = state.points[k].getData();
			
			bestCost = findCost(state, points, points[0], points[1]);
			
			bestCenters[k*2] = points[0];
			bestCenters[k*2+1] = points[1];
			
			//look through all the points in the cluster and find which has the minimum cost.
			//pick that point as the new medoid
			for(int i = 2; i < points.length; i+=2){
				cost = findCost(state, points, points[i],points[i+1]);
				
				if(cost < bestCost) {
					bestCost = cost;
					bestCenters[k*2] = points[i];
					bestCenters[k*2+1] = points[i+1];
				}
			}
		}
		return bestCenters;
	}
	
	/**
	 * Divide points into clusters based on medoids.
	 * @return New division. 
	 * Array where each array element indicates which cluster 
	 * the point corresponding to this array index belongs to. 
	 */
	static ArrayList<Float>[] dividePoints(PAMState state) {
		
		@SuppressWarnings("unchecked")
		ArrayList<Float>[] division = new ArrayList[state.k];
		for (int j = 0; j < state.k; j++) {
			division[j] = new ArrayList<Float>();
		}
		
		for (int k = 0; k < state.end-state.start; k++) {
			float distance = 0;
			float min;
			int bestIndex = 0;
			for (int i = 0; i < state.points[k].size(); i+=2) {
				min = Float.MAX_VALUE;
				for (int j = 0; j < state.medoids.size(); j+=2) {
					distance = PAMMath.distance(state.points[k].getData()[i], state.points[k].getData()[i+1], state.medoids.getData()[j], state.medoids.getData()[j+1]);
					
					//if distance is smaller then update minimum distance
					if(distance < min) {
						min = distance;
						bestIndex = j;
					}
				}
				
				division[bestIndex/2].add(state.points[k].getData()[i]);
				division[bestIndex/2].add(state.points[k].getData()[i+1]);
			}
			//for(int j = 0; j < state.k; j++) {
			//	System.err.println("distributed "+(division[j].size()/2)+" points to "+j+" from  "+k);
			//}
		}
		return division;
	}
	
	static float[] genPointShape(int number) {	
		int n = number/8;
		
		float[] points = new float[number*2];
		
		Random random = new Random(77777777);
		
		int j = 0;
		
		for (int i = 0; i < n; i++,j+=2) {
			float x = random.nextFloat()*100;
			float y = random.nextFloat()*100;	
			points[j] = x;
			points[j+1] = y;
		}
		
		for (int i = 0; i < n; i++,j+=2) {
			float x = random.nextFloat()*25;
			float y = random.nextFloat()*25;
			points[j] = x;
			points[j+1] = y;
		}
		
		for (int i = 0; i < n; i++,j+=2) {
			float x = random.nextFloat()*30+45;
			float y = random.nextFloat()*20+50;	
			points[j] = x;
			points[j+1] = y;
		}
		
		for (int i = 0; i < n; i++,j+=2) {
			float x = random.nextFloat()*30+70;
			float y = random.nextFloat()*20+80;	
			points[j] = x;
			points[j+1] = y;
		}
		
		for (int i = 0; i < n; i++,j+=2) {
			float x = random.nextFloat()*30+5;
			float y = random.nextFloat()*25+70;	
			points[j] = x;
			points[j+1] = y;
		}
		
		for (int i = 0; i < n; i++,j+=2) {
			float x = random.nextFloat()*20+65;
			float y = random.nextFloat()*30+5;	
			points[j] = x;
			points[j+1] = y;
		}
		
		for (int i = 0; i < n*2; i++,j+=2) {
			float x = random.nextFloat()*25+35;
			float y = random.nextFloat()*15+30;	
			points[j] = x;
			points[j+1] = y;
		}
		System.out.println("points total:" + points.length/2);
		return points;
	}
}
