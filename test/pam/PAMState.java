package pam;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;


import shared.FloatArrayWritable;
import ut.ee.newt.job.BSPState;
import ut.ee.newt.job.JobContext;

public class PAMState extends BSPState {
	
	FloatArrayWritable points[], medoids, oldmedoids;
	
	int k, num, it, start, end, range;

	long initTime;
	
	public PAMState(JobContext context) {
		super(context);
		
		initTime = System.currentTimeMillis();
		
		k = 8;
		num = 3000;
		
		int rank = context.getPid();
		
		range = (k%context.getNumTasks() == 0) ? k/context.getNumTasks() : k/context.getNumTasks()+1;
		start = rank*range;
	    end = Math.min((rank+1)*range, k);
		
		points = new FloatArrayWritable[end-start];
		for (int k = 0; k < end-start; k++) {
			points[k] = new FloatArrayWritable();
		}
		medoids = new FloatArrayWritable(new float[k*2]);
		oldmedoids = new FloatArrayWritable(new float[k*2]);
		
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		for (int k = 0; k < range; k++) {
			points[k].readFields(in);
		}
		medoids.readFields(in);
		oldmedoids.readFields(in);
		
		k = in.readInt();
		num = in.readInt();
		it = in.readInt();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		for (int k = 0; k < range; k++) {
			points[k].write(out);
		}
		medoids.write(out);
		oldmedoids.write(out);
		
		out.writeInt(k);
		out.writeInt(num);
		out.writeInt(it);
	}

}
