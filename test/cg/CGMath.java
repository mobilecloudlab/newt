package cg;

import cg.SparseLaplacianMatrix;

public class CGMath {

	static void scalevector(double[] vector, double scale) {
		for (int i = 0; i < vector.length; i++) {
			vector[i] *= scale;
		}
	}
	
	static void addVector(double[] vector1, double[] vector2) {
		for (int i = 0; i < vector1.length; i++) {
			vector1[i] += vector2[i];
		}
	}
	
	static void addScaledVector(double[] vector1, double scale, double[] vector2) {
		for (int i = 0; i < vector1.length; i++) {
			vector1[i] += scale*vector2[i];
		}
	}
	
	static double dotProduct(double[] vector1, double[] vector2) {
		double sum = 0;
		for (int i = 0; i < vector1.length; i++) {
			sum += vector1[i]*vector2[i];
		}
		return sum;
	}
	
	static double tolerance(double[] vector1) {
		double max = 0;
		for (int i = 0; i < vector1.length; i++) {
			if (max < Math.abs(vector1[i]))
				max = Math.abs(vector1[i]);
		}
		return max;
	}
	
	static void matVecMult(CGState state, double v[], double b[]) {
		for (int i = state.start; i < state.end; i++) {
			v[i-state.start] = state.laplacian.getElement(i, i)*b[i-state.start];
		}
		int row, col;
		for (int i = 0; i < state.laplacian.getRows().length; i++) {
			row = state.laplacian.getRows()[i]-state.start;
			col = state.laplacian.getColumns()[i];
			
			if (col-state.start < 0) {
				v[row] += SparseLaplacianMatrix.ADJ_VALUE*state.pre.getData()[col+state.overlap-state.start];
			} else if (col-state.start >= b.length) {
				v[row] += SparseLaplacianMatrix.ADJ_VALUE*state.post.getData()[col-state.end];
			} else {
				v[row] += SparseLaplacianMatrix.ADJ_VALUE*b[col-state.start];
			}
		}	
	}
}
