package cg;

public class SparseLaplacianMatrix {

	private int n, dim, N, N2, N3;
	private int[] rows, cols; 
	private double[] adjNodeCounts;
	
	public static double ADJ_VALUE = -1, r = 0;
	
	int start, end;

	public SparseLaplacianMatrix(int n, int dim) {
		this.n = n;
		this.dim = dim;
		
		N = n;
		N2 = n*n;
		N3 = N2*n;
		if (dim == 2)
			N = N2;
		else if (dim == 3)
			N = N3;
	}
	
	private int countAdjElements(int start, int end) {
		int elements = 0;
		for (int i = start; i < end; i++) {
			if (i+1 < N && (i+1)%n != 0)
				elements++;
			if (i-1 >= 0 && (i+1)%n != 1)
				elements++;
			if (i+n < N && (i+n)%N2 >= n)
				elements++;
			if (i-n >= 0 && (i)%N2 >= n)
				elements++;
			if (dim == 3) {
				if (i+N2 < N)
					elements++;
				if (i-N2 >= 0)
					elements++;
			}
		}
		
		return elements;
	}
	
	public void generate(int start, int end) {
		this.start = start;
		this.end = end;
		
		int e = countAdjElements(start, end), rindex = 0, cindex = 0;
		rows = new int[e];
		cols = new int[e];

		adjNodeCounts = new double[end-start];
		
		for (int i = start; i < end; i++) {
			if (i+1 < N && (i+1)%n != 0) {
				rows[rindex++] = i;
				cols[cindex++] = i+1;
			}
			if (i-1 >= 0 && (i+1)%n != 1) {
				rows[rindex++] = i;
				cols[cindex++] = i-1;
			}
			if (i+n < N && (i+n)%N2 >= n) {
				rows[rindex++] = i;
				cols[cindex++] = i+n;
			}
			if (i-n >= 0 && (i)%N2 >= n) {
				rows[rindex++] = i;
				cols[cindex++] = i-n;
			}
			if (dim == 3) {
				if (i+N2 < N) {
					rows[rindex++] = i;
					cols[cindex++] = i+N2;
				}
				if (i-N2 >= 0) {
					rows[rindex++] = i;
					cols[cindex++] = i-N2;
				}
			}
			
			adjNodeCounts[i-start] = 4;
			if (dim == 3)
				adjNodeCounts[i-start] += 2;
		}
	}
	
	public final int[] getRows() {
		return rows;
	}
	
	public final int[] getColumns() {
		return cols;
	}
	
	public double getElement(int i, int j) {
		double val = 0;
		if (i == j) {
			val = 4;
			if (dim == 3)
				val += 2;
		} else if (i+1 == j && (i+1)%n != 0 ||
				i-1 == j && (i+1)%n != 1||
				i+n == j && (i+n)%N2 >= n ||
				i-n == j && (i)%N2 >= n ||
				dim == 3 && (i+N2 == j || i-N2 == j)) {
			val = ADJ_VALUE;
		}
		
		return val;
	}

}
