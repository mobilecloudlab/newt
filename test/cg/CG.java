package cg;

import shared.DoubleArrayWritable;
import ut.ee.newt.comm.BSPComm;
import ut.ee.newt.job.Stage;

@SuppressWarnings("serial")
public class CG {
	
	static final double TOLERANCE = 10E-2;
	static final int MAX_IT = 1000;
	static int precondIters = 0;

	public static class Init extends Stage<CGState> {
		@Override
		public String execute(BSPComm bsp, CGState state, int superstep) {
			CGMath.matVecMult(state, state.r.getData(), state.x.getData());
			CGMath.scalevector(state.r.getData(),-1);
			CGMath.addVector(state.r.getData(), state.b.getData());
			
			state.error = Double.MAX_VALUE;
			
			return "beginLoop";
		}
	}
	
	public static class BeginLoop extends Stage<CGState> {
		
		@Override
		public String execute(BSPComm bsp, CGState state, int superstep) {
			if (state.it > 0) {
				double[] recv = getFromAll(bsp, 1);
				state.d += recv[0];
				
				double y = (state.d == 0) ? 0 : state.u/state.d;
				
				CGMath.addScaledVector(state.x.getData(), y, state.p.getData());
				CGMath.addScaledVector(state.r.getData(), -y, state.q.getData());
			}
			
			if (precondIters == 0) {
				for (int i = 0; i < state.z.size(); i++) {
					state.z.getData()[i] = state.r.getData()[i];
				}
			} else {
					SGSPreconditioner.run(state, state.z.getData(), state.r.getData(), precondIters);
			}
			
			state.ou = state.u;
			state.u = CGMath.dotProduct(state.r.getData(), state.z.getData());
			state.error = CGMath.tolerance(state.r.getData());
			
			sendToAll(bsp, new double[]{state.u, state.error});
			
			return "checkCondition";
		}
	}
	
	public static class CheckCondition extends Stage<CGState> {
		@Override
		public String execute(BSPComm bsp, CGState state, int superstep) {
			double[] recv = getFromAll(bsp, 2);
			state.u += recv[0];
			state.error = Math.max(state.error, recv[1]);
			//System.err.println("ERROR IS "+state.error);
			
			if (state.error > TOLERANCE && state.it <= MAX_IT) {
				if (state.it == 0) {
					for (int i = 0; i < state.p.size(); i++) {
						state.p.getData()[i] = state.z.getData()[i];
					}
				} else {
					double v = (state.ou == 0) ? 0 : state.u/state.ou;
					
					CGMath.scalevector(state.p.getData(), v);
					CGMath.addVector(state.p.getData(), state.z.getData());
				}
				
				sendToNeighbors(bsp, state, state.p.getData());
				
				return "doMatVec";
			} else {
				System.err.println("Finished with "+state.it+" iterations");
				System.out.println("cg time "+(System.currentTimeMillis()-state.initTime));
				//write result to disk here
				
				return Stage.STAGE_END;
			}
		}
	}
	
	public static class DoMatVec extends Stage<CGState> {
		@Override
		public String execute(BSPComm bsp, CGState state, int superstep) {
			getFromNeighbors(bsp, state);
			
			CGMath.matVecMult(state, state.q.getData(), state.p.getData());
			
			state.d = CGMath.dotProduct(state.p.getData(), state.q.getData());
			
			sendToAll(bsp, new double[]{state.d});

			state.it++;
			return "beginLoop";
		}
	}
	
	static void sendToAll(BSPComm comm, double[] array) {
		for (int pid = 0; pid < comm.nprocs(); pid++) {
			if (pid != comm.pid()) {
				comm.send(pid, new DoubleArrayWritable(array));
			}
		}
	}
	
	static double[] getFromAll(BSPComm comm, int n) {
		double[] array = new double[n];
		for (int pid = 0; pid < comm.nprocs(); pid++) {
			if (pid != comm.pid()) {
				double[] recv = ((DoubleArrayWritable)comm.move()).getData();
				array[0] += recv[0];
				if (recv.length  == 2) {
					array[1] = Math.max(array[1], recv[1]);
				}
			}
		}
		return array;
	}
	
	static void sendToNeighbors(BSPComm comm, CGState state, double[] array) {
		if (comm.pid() != 0) {
			DoubleArrayWritable msg = new DoubleArrayWritable(new double[state.overlap]);
			for (int i = 0; i < state.overlap; i++) {
				msg.getData()[i] = array[i];
			}
			comm.send(comm.pid()-1, msg);
		}
		if (comm.pid() != comm.nprocs()-1) {
			DoubleArrayWritable msg = new DoubleArrayWritable(new double[state.overlap]);
			for (int i = 0; i < state.overlap; i++) {
				msg.getData()[i] = array[array.length-state.overlap+i];
			}
			comm.send(comm.pid()+1, msg);
		}
	}
	
	static void getFromNeighbors(BSPComm comm, CGState state) {
		if (comm.pid() != 0) {
			double[] recv = ((DoubleArrayWritable)comm.move(comm.pid()-1)).getData();
			for (int i = 0; i < recv.length; i++) {
				state.pre.getData()[i] = recv[i];
			}
		}
		if (comm.pid() != comm.nprocs()-1) {
			double[] recv = ((DoubleArrayWritable)comm.move(comm.pid()+1)).getData();
			for (int i = 0; i < recv.length; i++) {
				state.post.getData()[i] = recv[i];
			}
		}
	}
}
