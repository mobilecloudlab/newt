package cg;

import cg.SparseLaplacianMatrix;

public class SGSPreconditioner {

	public static void run(CGState state, double x[], double b[], int it) {
		
		int N = state.mSize;
		int N2 = state.mSize*state.mSize;
		int N3 = N2*state.mSize;
		if (state.dim == 2)
			N = N2;
		else if (state.dim == 3)
			N = N3;
		
		if (it == 0) {
			for (int i = 0; i < x.length; i++) {
				x[i] = b[i];
			}
		} else {
			for (int i = 0; i < x.length; i++) {
				x[i] = 0;
			}
			try {
				/*if (synchronize) {
					sync(x);
				}*/
				
				//forward
				for (int k = 0; k < it; k++) {
					for (int i = state.start; i < state.end; i++) {
						x[i-state.start] = (b[i-state.start]-getSum(state, i, x, N, N2))/state.laplacian.getElement(i, i);
					}
				}
				/*if (synchronize) {
					sync(x);
				}*/
				//backward
				for (int k = 0; k < it; k++) {
				    for (int i = state.end-1; i >= state.start; i--) {
				    	x[i-state.start] = (b[i-state.start]-getSum(state, i, x, N, N2))/state.laplacian.getElement(i, i);
				    }
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				System.exit(-1);
			}
		}
	}
	
	/*private final void sync(double[] x) {
		if (state > 1) {
			CG.sendToNeighbors(comm, state, array);
		}
	}*/
	
	private final static double getSum(CGState state, int i, double[] x, int N, int N2) {
		double sum = 0;
		
		if (i+1 < N && (i+1)%state.mSize != 0)
			sum += getSumHelper(state, i+1, x);
		if (i-1 >= 0 && (i+1)%state.mSize != 1)
			sum += getSumHelper(state, i-1, x);
		if (i+state.mSize < N && (i+state.mSize)%N2 >= state.mSize)
			sum += getSumHelper(state, i+state.mSize, x);
		if (i-state.mSize >= 0 && (i)%N2 >= state.mSize)
			sum += getSumHelper(state, i-state.mSize, x);
		if (state.dim == 3) {
			if (i+N2 < N) {
				sum += getSumHelper(state, i+N2, x);
			}
			if (i-N2 >= 0) {
				sum += getSumHelper(state, i-N2, x);
			}
		}
		
		return sum;
	}

	private final static double getSumHelper(CGState state, int col, double[] x) {
		double sum = 0;
		if (col-state.start < 0) {
			//sum += SparseLaplacianMatrix.ADJ_VALUE*state.pre.getData()[col+state.overlap-state.start];
		} else if (col-state.start >= x.length) {
			//sum += SparseLaplacianMatrix.ADJ_VALUE*state.post.getData()[col-state.end];
		} else {
			sum += SparseLaplacianMatrix.ADJ_VALUE*x[col-state.start];
		}
		
		return sum;
	}
}
