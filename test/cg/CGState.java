package cg;

import cg.SparseLaplacianMatrix;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
//import java.util.Random;

import shared.DoubleArrayWritable;
import ut.ee.newt.job.BSPState;
import ut.ee.newt.job.JobContext;


public class CGState extends BSPState {
	
	DoubleArrayWritable r, z, p, q, x, b, pre, post;
	
	int range, start, end;
	
	SparseLaplacianMatrix laplacian;
	
	int it, overlap;
	double ou, u, d, error;
	
	long initTime;
	
	int dim, mSize, N;
	
	public CGState(JobContext context) {
		super(context);
		
		initTime = System.currentTimeMillis();
		
		dim = 3;
		mSize = 30;
	    N = (int)Math.pow(mSize, dim);
		range = N/context.getNumTasks()+1;
		start = (context.getPid())*range;
	    end = Math.min((context.getPid()+1)*range, N);
	    
	    laplacian = new SparseLaplacianMatrix(mSize, dim);
	    overlap = (int)(Math.pow(mSize, dim-1));
	    
	    laplacian.generate(start, end);
	    
	    int n = end-start;
		r = new DoubleArrayWritable(new double[n]);
		z = new DoubleArrayWritable(new double[n]);
		p = new DoubleArrayWritable(new double[n]);
		q = new DoubleArrayWritable(new double[n]);
		x = new DoubleArrayWritable(new double[n]);
		b = new DoubleArrayWritable(new double[n]);
		
		//Random random = new Random();
		for (int i = 0; i < b.size(); i++) {
			b.getData()[i] = 10;
			x.getData()[i] = 0;
		}
		
		int preSize = overlap, postSize = overlap;
		
		if (context.getPid() == 0)
			preSize = 0;
		if (context.getPid() == context.getNumTasks()-1)
			postSize = 0;
		
		pre = new DoubleArrayWritable(new double[preSize]);
		post = new DoubleArrayWritable(new double[postSize]);
		
		u = Double.MAX_VALUE;
		
		long usedKB = (Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory())/1024;
		System.err.println("Memory usage: "+usedKB+" kb. ");
	}

	@Override
	public void write(DataOutput out) throws IOException {
		r.write(out);
		z.write(out);
		p.write(out);
		q.write(out);
		x.write(out);
		b.write(out);
		
		out.writeInt(it);
		out.writeDouble(ou);
		out.writeDouble(u);
		out.writeDouble(d);
		
		pre.write(out);
		post.write(out);
		
		out.writeDouble(error);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		r.readFields(in);
		z.readFields(in);
		p.readFields(in);
		q.readFields(in);
		x.readFields(in);
		b.readFields(in);
		
		it = in.readInt();
		ou = in.readDouble();
		u = in.readDouble();
		d = in.readDouble();
		
		pre.readFields(in);
		post.readFields(in);
		
		error = in.readDouble();
	}
	
}