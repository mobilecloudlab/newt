package test;

import ut.ee.newt.NEWTClient;

public class TestJob {
	
	public static void main(String[] args) {
		NEWTClient client = new NEWTClient();
		client.submit(CGTestMaster.class, args, true);
		client.close();
	}

}
