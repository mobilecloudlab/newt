package test;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;

import pam.PAM;
import pam.PAMState;
import shared.FloatArrayWritable;
import ut.ee.newt.NEWTConfiguration;
import ut.ee.newt.job.master.impl.FaultTolerantJobMaster;
import ut.ee.newt.job.master.input.Input;
import ut.ee.newt.job.master.input.impl.NullInput;

public class PAMTestMaster {
	public static void main(String[] args) {
		Configuration conf = NEWTConfiguration.createDefault();
		//Input input = new HDFSFileInput(conf,"/user/ubuntu/in");
		Input input = new NullInput(Integer.parseInt(args[0]));
		FaultTolerantJobMaster<PAMState> am =
				new FaultTolerantJobMaster<PAMState>(conf, PAMState.class, input);
		am.addStage("scatterPoints",		new PAM.ScatterPoints());
		am.addStage("scatterMedoids",		new PAM.ScatterMedoids());
		am.addStage("dividePoints",			new PAM.DividePoints());
		am.addStage("recalculateMedoids",	new PAM.RecalculateMedoids());
		am.registerMessageType(FloatArrayWritable.class);
		try {
			am.run();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}