package shared;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

public class FloatArrayWritable implements Writable {
    private float[] data;
    
    public int tag;

    public FloatArrayWritable() {

    }

    public FloatArrayWritable(float[] data) {
        this.data = data;
    }

    public float[] getData() {
        return data;
    }

    public void setData(float[] data) {
        this.data = data;
    }
    
    public int size() {
    	return data.length;
    }

    public void write(DataOutput out) throws IOException {
        int length = 0;
        if(data != null) {
            length = data.length;
        }

        out.writeInt(length);
        out.writeInt(tag);

        for(int i = 0; i < length; i++) {
            out.writeFloat(data[i]);
        }
    }

    public void readFields(DataInput in) throws IOException {
        int length = in.readInt();
        tag = in.readInt();
        
        data = new float[length];

        for(int i = 0; i < length; i++) {
            data[i] = in.readFloat();
        }
    }
}
