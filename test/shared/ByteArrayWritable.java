package shared;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

public class ByteArrayWritable implements Writable {
    private byte[] data;

    public ByteArrayWritable() {

    }

    public ByteArrayWritable(byte[] data) {
        this.data = data;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
    
    public int size() {
    	return data.length;
    }

    public void write(DataOutput out) throws IOException {
        int length = 0;
        if(data != null) {
            length = data.length;
        }

        out.writeInt(length);

        for(int i = 0; i < length; i++) {
            out.writeByte(data[i]);
        }
    }

    public void readFields(DataInput in) throws IOException {
        int length = in.readInt();
        
        data = new byte[length];

        for(int i = 0; i < length; i++) {
            data[i] = in.readByte();
        }
    }
}
