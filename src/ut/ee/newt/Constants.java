/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt;

public class Constants {

	public static final String NEWT_LOCAL_LOG_DIR = "newt.output.dir";
	public static final String NEWT_INPUT_DIR = "newt.input.dir";
	public static final String NEWT_NUM_TASKS = "newt.num.tasks";
	public static final String NEWT_PORT_START = "newt.port.start";
	public static final String NEWT_MASTER_PORT = "newt.master.port";
	public static final String NEWT_JOB_TMP_DIR = "newt.tmp.dir";
	public static final String NEWT_PROCESS_MEMORY = "newt.process.memory";
	public static final String NEWT_MASTER_MEMORY = "newt.master.memory";

}
