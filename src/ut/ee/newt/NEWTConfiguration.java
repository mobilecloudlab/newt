/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt;

import java.util.concurrent.TimeUnit;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.retry.RetryPolicies;
import org.apache.hadoop.io.retry.RetryPolicy;

public class NEWTConfiguration extends Configuration {

	private NEWTConfiguration() {
		addResource("newt-default.xml");
	}
	
	public static Configuration createDefault() {
		return new NEWTConfiguration();
	}
	
	public static RetryPolicy getContainerRequestRetryPolicy(Configuration conf) {
		int attamptInterval = conf.getInt("newt.container.request.interval", 500);
		int numRetries = conf.getInt("newt.container.request.retries", 10);
		return RetryPolicies.retryUpToMaximumTimeWithFixedSleep(attamptInterval*numRetries, attamptInterval, TimeUnit.MILLISECONDS);
	}
	
	public static RetryPolicy getConnectionSetupRetryPolicy(Configuration conf) {
		int attamptInterval = conf.getInt("newt.connection.setup.interval", 2000);
		int numRetries = conf.getInt("newt.connection.setup.retries", 5);
		return RetryPolicies.retryUpToMaximumTimeWithFixedSleep(attamptInterval*numRetries, attamptInterval, TimeUnit.MILLISECONDS);
	}
}
