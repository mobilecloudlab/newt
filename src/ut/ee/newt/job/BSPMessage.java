/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.job;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;
import org.apache.log4j.Logger;

public class BSPMessage {

	public int source, target, superstep;
	public Writable payload;
	
	private static final Logger LOG = Logger.getLogger(BSPMessage.class);
	
	public BSPMessage(int source, int target, int superstep, Writable payload) {
		this.target = target;
		this.source = source;
		this.payload = payload;
		this.superstep = superstep;
	}

	public BSPMessage() {
		
	}
	
	public Class<? extends Writable> getMessageType() {
		return payload.getClass();
	}

	public void write(DataOutput out) throws IOException {
		payload.write(out);
	}
	
	public static BSPMessage create(DataInput in, int source, int target, int superstep, Class<? extends Writable> messageClass) {
		BSPMessage msg = new BSPMessage();
		msg.source = source;
		msg.target = target;
		msg.superstep = superstep;
		try {
			msg.payload = messageClass.newInstance();
			msg.payload.readFields(in);
		} catch (Exception e) {
			LOG.error(e.getMessage());
			e.printStackTrace();
		}
		return msg;
	}
	
	@Override
	public String toString() {
		return "("+superstep+")"+source+"->"+target+" '"+payload+"'";
	}
}
