/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.job.master;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.hadoop.yarn.api.protocolrecords.AllocateResponse;
import org.apache.hadoop.yarn.api.records.ContainerId;
import org.apache.hadoop.yarn.api.records.ContainerStatus;
import org.apache.log4j.Logger;

import ut.ee.newt.misc.Packet;
import ut.ee.newt.misc.ProcessId;

public class ContainerMonitor {
	private static final Logger LOG = Logger.getLogger(ContainerMonitor.class);
	
    int numTotalContainers;

	private AtomicInteger numCompletedContainers = new AtomicInteger();
	private AtomicInteger numFailedContainers = new AtomicInteger();
	
	private List<ProcessId> requests;
	private Map<ContainerId, ProcessId> allocations;
	
	private List<ProcessId> unallocated;
	
	private Map<Integer, ProcessId> pids;
	
	private ApplicationMaster am;
	
	private boolean done;
	
	ContainerMonitor(ApplicationMaster am, int numProcs) {
		this.am = am;
		this.numTotalContainers = numProcs;

		unallocated = new ArrayList<ProcessId>();
		pids = new HashMap<Integer,ProcessId>();
		for (int i = 0; i < numProcs; i++) {
			ProcessId pid = new ProcessId(null, i);
			pids.put(pid.getId(), pid);
			unallocated.add(pid);
		}
		
		requests = new ArrayList<ProcessId>();
		allocations = new HashMap<ContainerId, ProcessId>();
		
		done = false;
	}
	
	public void registerRequested(ProcessId pid) {
		requests.add(pid);
	}
	
	public void unregisterRequested(ProcessId pid) {
		requests.remove(pid);
	}
	
	public boolean isRequested(ProcessId pid) {
		return requests.contains(pid);
	}
	
	public void registerAllocated(ContainerId id, ProcessId pid) {
		unregisterRequested(pid);
		allocations.put(id, pid);
		pid.setContainerId(id);
	}
	
	public void unregisterAllocated(ContainerId id) {
		unallocated.add(allocations.get(id));
		allocations.remove(id);
	}
	
	public boolean isAllocated(ContainerId id) {
		return allocations.containsKey(id);
	}

	public int getFailed() {
		return numFailedContainers.get();
	}

	public int getRequested() {
		return requests.size();
	}

	public int getCompleted() {
		return numCompletedContainers.get();
	}

	public int getAllocated() {
		return allocations.size();
	}

	public boolean done() {
		return done;
	}
	
	public boolean allocationsComplete() {
		return unallocated.isEmpty();
	}

	public void update(AllocateResponse amResp) {
		List<ContainerStatus> completedContainers = amResp.getCompletedContainersStatuses();
		for (ContainerStatus containerStatus : completedContainers) {
			int exitStatus = containerStatus.getExitStatus();
			//LOG.info("Completed container status: "+containerStatus.getState());
			if (exitStatus != 0) {
				handleProcessFailure(containerStatus);
			} else {
				numCompletedContainers.incrementAndGet();
			}
		}
		if (numCompletedContainers.get() == numTotalContainers)
			done = true;
	}
	
	private void handleProcessFailure(ContainerStatus containerStatus) {
		//synchronized (am.barrierExitStatus) {
		int exitStatus = containerStatus.getExitStatus();
		ProcessId failedProcess = allocations.get(containerStatus.getContainerId());
		if (failedProcess != null) {
			if (exitStatus == -100) {
				initializeRecovery(containerStatus.getContainerId(), failedProcess);
			} else {
				if (failedProcess.getAttemptId() >= 1) {
					numCompletedContainers.incrementAndGet();
					numFailedContainers.incrementAndGet();
					LOG.info("Retry attempt limit exceeded, failing process "+failedProcess+" indefinitely");
					done = true;
				} else {
					initializeRecovery(containerStatus.getContainerId(), failedProcess);
				}
			}
		}
		//}
	}

	private void initializeRecovery(ContainerId failedContainer, ProcessId failedProcess) {
		LOG.info("Initializing recovery due to process with "+failedProcess+" failing.");
		failedProcess.setAttemptId(failedProcess.getAttemptId()+1);
		unregisterAllocated(failedContainer);
		am.barrierExitStatus = Packet.RECOVERY_NEEDED;
	}
	
	public int getProgress() {
		return numCompletedContainers.get()/numTotalContainers;
	}

	public Map<Integer, ProcessId> getPids() {
		return pids;
	}

	List<ProcessId> getUnallocated() {
		return unallocated;
	}

}
