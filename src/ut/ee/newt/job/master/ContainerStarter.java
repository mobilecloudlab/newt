/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.job.master;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.yarn.api.records.Container;
import org.apache.hadoop.yarn.api.records.ContainerLaunchContext;
import org.apache.hadoop.yarn.api.records.ContainerState;
import org.apache.hadoop.yarn.api.records.ContainerStatus;
import org.apache.hadoop.yarn.client.api.impl.NMClientImpl;
import org.apache.hadoop.yarn.exceptions.YarnException;
import org.apache.log4j.Logger;

import ut.ee.newt.Constants;
import ut.ee.newt.job.Job;
import ut.ee.newt.job.JobRunner;
import ut.ee.newt.misc.Launcher;
import ut.ee.newt.misc.ProcessId;
import ut.ee.newt.misc.Utils;
import ut.ee.newt.misc.impl.RemoteLauncher;

class ContainerStarter extends NMClientImpl implements Runnable {
	
	private static final Logger LOG = Logger.getLogger(ContainerStarter.class);
	
	Container container;

	private Launcher launcher;
	
	private Class<? extends Job> jobClass;
	
	ProcessId pid;
	Configuration conf;

	public ContainerStarter(Container container, Configuration conf, ProcessId pid, Class<? extends Job> jobClass) {
		this.conf = conf;
		this.jobClass = jobClass;
		this.container = container;
		this.launcher = new RemoteLauncher(conf);
		this.pid = pid;
		
		init(conf);
	}
	
	@Override
	public void run() {
		start();
		ContainerLaunchContext context = Utils.createContainerLaunchContext();
		launcher.setOutLogPath(conf.get(Constants.NEWT_LOCAL_LOG_DIR)+"/out-"+pid.getId()+"-"+pid.getAttemptId());
		launcher.setErrLogPath(conf.get(Constants.NEWT_LOCAL_LOG_DIR)+"/log-"+pid.getId()+"-"+pid.getAttemptId());
		try {
			launcher.setup(context, JobRunner.class, buildArgList(), conf.getInt(Constants.NEWT_PROCESS_MEMORY, 256));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}

		LOG.info("Starting container "+container.getId()+" for pid "+pid.getId());
		try {
			startContainer(container, context);
		} catch (YarnException e) {
			LOG.error(e.getMessage());
		} catch (IOException e) {
			LOG.error(e.getMessage());
		}
	}
	
	public boolean isRunning() {
		boolean running = false;
		ContainerStatus response = null;
		try {
			response = getContainerStatus(container.getId(), container.getNodeId());
			if (response.getState().equals(ContainerState.RUNNING))
				running = true;
		} catch (YarnException e) {
			//LOG.error(e.getMessage());
		} catch (IOException e) {
			LOG.error(e.getMessage());
		}
		return running;
	}
	
	public void stop() {
		LOG.info("Attempting to forcefully kill process "+pid);
		try {
			stopContainer(container.getId(), container.getNodeId());
		} catch (YarnException e) {
			//LOG.error(e.getMessage());
		} catch (IOException e) {
			LOG.error(e.getMessage());
		}
	}

	String[] buildArgList() throws UnknownHostException {
		List<String> args = new ArrayList<String>();
		args.add(jobClass.getName());
		args.add(Integer.toString(pid.getId()));
		
		int tasks = conf.getInt(Constants.NEWT_NUM_TASKS, 14000);
		args.add(Integer.toString(tasks));
		String localhostname = InetAddress.getLocalHost().getHostName();
		args.add(localhostname);
		args.add(Integer.toString(conf.getInt(Constants.NEWT_MASTER_PORT, 13999)));
		args.add(pid.toString());
		
		return args.toArray(new String[args.size()]);
	}
	
}