/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.job.master.impl;

import org.apache.hadoop.conf.Configuration;

import ut.ee.newt.Constants;
import ut.ee.newt.job.Job;
import ut.ee.newt.job.master.ApplicationMaster;
import ut.ee.newt.job.master.input.Input;
import ut.ee.newt.misc.Packet;

public class JobWithInputMaster extends ApplicationMaster {
	
	Input input;
	
	JobWithInputMaster(Configuration conf, Input input, Class<? extends Job> jobClass) {
		super(conf, jobClass);
		this.input = input;
		
		conf.setInt(Constants.NEWT_NUM_TASKS, input.initialize());
		
		registerRequestHandler(Packet.GET_INPUT, new RequestResponse() {
			@Override
			public Object respond(int pid) {
				Object input = getInput().getForProcess(pid);
				return input != null ? input : "";
			}
        });
	}
	
	Input getInput() {
		return input;
	}

}
