/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.job.master.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.log4j.Logger;

import ut.ee.newt.Constants;
import ut.ee.newt.job.BSPState;
import ut.ee.newt.job.Stage;
import ut.ee.newt.job.impl.FaultTolerantJob;
import ut.ee.newt.job.impl.NullState;
import ut.ee.newt.job.master.input.Input;
import ut.ee.newt.misc.Packet;

public class FaultTolerantJobMaster<S extends BSPState> extends JobWithInputMaster {
	
	private static final Logger LOG = Logger.getLogger(FaultTolerantJobMaster.class);

	private Map<String, Stage<S>> stages;
	
	public FaultTolerantJobMaster(Configuration conf, Class<S> stateClass, Input input) {
		super(conf, input, FaultTolerantJob.class);

		conf.setClass(FaultTolerantJob.STATE_CLASS, stateClass, BSPState.class);
        stages = new HashMap<String,Stage<S>>();
        addStage(Stage.STAGE_END, null);
        
        registerRequestHandler(Packet.GET_COMPUTATION_STAGES, new RequestResponse() {
			@Override
			public Object respond(int pid) {
				return stages;
			}
        });
        registerRequestHandler(Packet.GET_INITIAL_STAGE, new RequestResponse() {
			@Override
			public Object respond(int pid) {
				return getConfiguration().get(Stage.INITIAL_STAGE);
			}
        });
        registerRequestHandler(Packet.GET_STATE_CLASS, new RequestResponse() {
			@Override
			public Object respond(int pid) {
				return getConfiguration().getClass(FaultTolerantJob.STATE_CLASS, NullState.class, BSPState.class);
			}
        });
		registerRequestHandler(Packet.GET_CHECKPOINT_DIR, new RequestResponse() {
			@Override
			public Object respond(int pid) {
				return getConfiguration().get(Constants.NEWT_JOB_TMP_DIR);
			}
        });
		registerRequestHandler(Packet.GET_RECOVERY_NEEDED, new RequestResponse() {
			@Override
			public Object respond(int pid) {
				Packet response = Packet.NULL_RESPONSE;
				if (monitor.getPids().get(pid).getAttemptId() > 0)
					response = Packet.RECOVERY_NEEDED;
				return response;
			}
        });
		registerRequestHandler(Packet.GET_CURRENT_SUPERSTEP, new RequestResponse() {
			@Override
			public Object respond(int pid) {
				return superstep.get();
			}
        });
	}

	public void addStage(String name, Stage<S> stage) {
		if (stages.size() == 1)
			setInitialStage(name);
		stages.put(name, stage);
	}
	
	public void setInitialStage(String stageName) {
		LOG.info("Set initial stage to '"+stageName+"'");
		getConfiguration().set(Stage.INITIAL_STAGE, stageName);
	}
}
