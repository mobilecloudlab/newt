/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.job.master;

import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.ArrayPrimitiveWritable;
import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.ByteWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.ObjectWritable;
import org.apache.hadoop.io.ShortWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.TwoDArrayWritable;
import org.apache.hadoop.io.VIntWritable;
import org.apache.hadoop.io.VLongWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.retry.RetryPolicy;
import org.apache.hadoop.io.retry.RetryPolicy.RetryAction;
import org.apache.hadoop.yarn.api.protocolrecords.AllocateResponse;
import org.apache.hadoop.yarn.api.protocolrecords.RegisterApplicationMasterResponse;
import org.apache.hadoop.yarn.api.records.Container;
import org.apache.hadoop.yarn.api.records.FinalApplicationStatus;
import org.apache.hadoop.yarn.api.records.Priority;
import org.apache.hadoop.yarn.api.records.Resource;
import org.apache.hadoop.yarn.exceptions.YarnException;
import org.apache.log4j.Logger;

import org.apache.hadoop.yarn.client.api.AMRMClient;
import org.apache.hadoop.yarn.client.api.AMRMClient.ContainerRequest;
import org.apache.hadoop.yarn.client.api.impl.AMRMClientImpl;

import ut.ee.newt.Constants;
import ut.ee.newt.NEWTConfiguration;
import ut.ee.newt.comm.MasterCommunicator;
import ut.ee.newt.comm.mina.MINAMasterCommunicator;
import ut.ee.newt.job.Job;
import ut.ee.newt.misc.Packet;
import ut.ee.newt.misc.ProcessId;

public class ApplicationMaster {
	
	private AMRMClient<ProcessContainerRequest> client; 
	
	public static class ProcessContainerRequest extends ContainerRequest {

		static final String[] LOCATION_LOCAL = new String[] {"localhost"};
		static final String[] LOCATION_ANY = null;//new String[] {"*"};
		
		public ProcessContainerRequest(Configuration conf, int priority) {
			super(Resource.newInstance(conf.getInt(Constants.NEWT_PROCESS_MEMORY, 256), 2),
					LOCATION_ANY, null, Priority.newInstance(priority));
		}
	}
	
	public static interface RequestResponse {

		public Object respond(int pid);
	}
	
	private static final Logger LOG = Logger.getLogger(ApplicationMaster.class);
	
	//private ContainerId containerId;
	//private ApplicationAttemptId appAttemptId;
	
	protected ContainerMonitor monitor;
	
	public AtomicBoolean hostsReady = new AtomicBoolean();
	
	private Class<? extends Job> jobClass;
	
	Configuration conf;
	
	private BidiMap hosts, messageTypes;
	
	private Map<Packet, RequestResponse> requestHandlers;
	
	public Packet barrierExitStatus;
	
	public AtomicInteger superstep = new AtomicInteger();
	
	MasterCommunicator comm;
	
	protected void registerRequestHandler(Packet request, RequestResponse response) {
		LOG.debug("Registered RequestResponse for "+request);
		requestHandlers.put(request, response);
	}
	
	private ApplicationMaster(Configuration conf) {
		this.conf = conf;

        requestHandlers = new HashMap<Packet,RequestResponse>();
        registerRequestHandler(Packet.GET_MESSAGE_TYPES, new RequestResponse() {
			@Override
			public Object respond(int pid) {
	        	return messageTypes;
			}
        });
        
        messageTypes = new DualHashBidiMap();
        registerCommonTypes();
		
		barrierExitStatus = Packet.EXITED_BARRIER;
	
		comm = MINAMasterCommunicator.create(this);
		client = new AMRMClientImpl<ProcessContainerRequest>();
		client.init(conf);
	}
	
	public void handleErronousExit(Throwable ex) {
		ex.printStackTrace();
		try {
			client.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			cleanTmp(getConfiguration());
			System.exit(-1);
		}
	}
	
	public ApplicationMaster(Configuration conf, Class<? extends Job> jobClass) {
		this(conf);
		this.jobClass = jobClass;
		
		Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler(){
			@Override
			public void uncaughtException(Thread t, Throwable ex) { handleErronousExit(ex); }
		});
		/*
		Map<String, String> envs = System.getenv();
	    String containerIdString = envs.get(ApplicationConstants.AM_CONTAINER_ID_ENV);
	    if (containerIdString == null) {
	    	throw new IllegalArgumentException("ContainerId not set in the environment");
	    }
	    containerId = ConverterUtils.toContainerId(containerIdString);
	    appAttemptId = containerId.getApplicationAttemptId();
	    */
		conf.set(Constants.NEWT_JOB_TMP_DIR,
				Path.SEPARATOR+"user"+Path.SEPARATOR+
				System.getProperty("user.name")+Path.SEPARATOR+
				"tmp_0");//+appAttemptId.getApplicationId());
        
        hosts = new DualHashBidiMap();
        hostsReady.set(false);
	}
	
	
	public Map<Packet,RequestResponse> getRequestHandlers() {
		return requestHandlers;
	}
	
	private void registerCommonTypes() {
        registerMessageType(NullWritable.class);
        registerMessageType(ObjectWritable.class);
        registerMessageType(Text.class);
        registerMessageType(ByteWritable.class);
        registerMessageType(BooleanWritable.class);
        registerMessageType(ShortWritable.class);
        registerMessageType(IntWritable.class);
        registerMessageType(VIntWritable.class);
        registerMessageType(LongWritable.class);
        registerMessageType(VLongWritable.class);
        registerMessageType(FloatWritable.class);
        registerMessageType(DoubleWritable.class);
        registerMessageType(IntWritable.class);
        registerMessageType(TwoDArrayWritable.class);
        registerMessageType(ArrayWritable.class);
        registerMessageType(ArrayPrimitiveWritable.class);
	}
	
	public BidiMap getHosts() {
		return hosts;
	}
	
	public void registerMessageType(Class<? extends Writable> messageClass) {
		messageTypes.put((byte)messageTypes.size(), messageClass.getName());
	}
	
	class ContainerAllocationHandler {
		int fails = 0, failovers = 0, port;
    	RetryPolicy policy;
    	
		List<Container> newlyAllocatedContainers;
		ContainerStarter[] containerLaunchers;
		
		Exception ex = new Exception("Could not allocate sufficient amount of containers");
		
		ContainerAllocationHandler(int numProcs) {
	    	newlyAllocatedContainers = new ArrayList<Container>();
	    	containerLaunchers = new ContainerStarter[numProcs];
	    	port = conf.getInt(Constants.NEWT_PORT_START, 14000);
	    	policy = NEWTConfiguration.getContainerRequestRetryPolicy(conf);
		}
		
		private void handleNewContainerAllocation(AllocateResponse amResp) {
			List<Container> allocated = amResp.getAllocatedContainers();
			int allocatedContainers = allocated.size();
			LOG.info("Got "+allocatedContainers+" containers from resource manager");
			for (int i = 1; i <= allocatedContainers; i++) {
				if (i > monitor.getUnallocated().size()) {
					LOG.info("Got an extra container (?), releasing");
					client.releaseAssignedContainer(allocated.get(0).getId());
				} else {
					newlyAllocatedContainers.add(allocated.get(0));
				}
				allocated.remove(0);
    		}
			if (monitor.getUnallocated().size() - newlyAllocatedContainers.size() <= 0) {
				for (int i = 0; i < monitor.getUnallocated().size(); i++) {
					Container container = newlyAllocatedContainers.get(0);
					ProcessId pid = monitor.getUnallocated().get(i);
		    		ContainerStarter starter = new ContainerStarter(container, conf, pid, jobClass);
		    		hosts.put(pid.getId(), container.getNodeId().getHost()+":"+(port++));
		    		monitor.registerAllocated(container.getId(), pid);
		    		newlyAllocatedContainers.remove(container);
		    		
		    		Thread launchThread = new Thread(starter);
		    	    containerLaunchers[pid.getId()] = starter;
		    		launchThread.start();
		    	}
				monitor.getUnallocated().clear();
		    	//newlyAllocatedContainers.clear();
		    	if (monitor.allocationsComplete()) {
		    		hostsReady.set(true);
		    	}
			} else {
	    		handleAllocationFailure();
			}
		}

		private void handleAllocationFailure() {
			try {
				synchronized (this) {
		    		RetryAction ra = policy.shouldRetry(ex, ++fails, failovers, false);
		    		if (ra == RetryAction.FAILOVER_AND_RETRY) failovers++;
					if (ra == RetryAction.FAIL)
						throw ex;
					wait(ra.delayMillis);
					LOG.info("Waiting for "+
							(monitor.getUnallocated().size() - newlyAllocatedContainers.size())+
							" containers");
					return;
				}
			} catch (Exception err) {
				LOG.error(err.getMessage());
				err.printStackTrace();
				System.exit(-1);
			}
		}

		public void stopContainers() {
			for (ContainerStarter cs : containerLaunchers)
    			if (cs.isRunning()) cs.stop();
		}
		
	}

	public boolean run() throws IOException {
		client.start();
		int numProcs = conf.getInt(Constants.NEWT_NUM_TASKS, 0);
	    comm.init(conf);
	    monitor = new ContainerMonitor(this, numProcs);
	    
	    RegisterApplicationMasterResponse response = null;
	    try {
			response = client.registerApplicationMaster(InetAddress.getLocalHost().getHostAddress(), 8099, "");
		} catch (YarnException ex) {
			handleErronousExit(ex);
		} catch (IOException ex) {
			handleErronousExit(ex);
		}
	    LOG.info("Cluster status: "+response);
	    
    	List<ProcessContainerRequest> resourceReqs = new ArrayList<ProcessContainerRequest>();
    	ContainerAllocationHandler allocationHandler = new ContainerAllocationHandler(numProcs);
    	
	    while (true) {
	    	if (monitor.done()) {
	    		allocationHandler.stopContainers();
	    		break;
	    	}
	    	
	    	try {
	    		Thread.sleep(500);
	    	} catch (InterruptedException e) {
	    		e.printStackTrace();
	    	}

    		resourceReqs.clear();
	    	
	    	int p = 0;
	    	for (ProcessId pid : monitor.getUnallocated()) {
	    		if (!monitor.isRequested(pid)) {
	    			LOG.info("Requesting container for pid "+pid.getId());
	    			resourceReqs.add(new ProcessContainerRequest(conf, p++));
	    			monitor.registerRequested(pid);
	    		}
	    	}
	    	if (!resourceReqs.isEmpty())
    			hostsReady.set(false);
	    	
	    	AllocateResponse amResp = null;
	    	try {
	    		for (ProcessContainerRequest request : resourceReqs)
	    			client.addContainerRequest(request);
	    		amResp = client.allocate(monitor.getProgress());
			} catch (YarnException e) {
				LOG.error(e.getMessage());
				cleanTmp(conf);
				System.exit(-1);
			}
	    	if (!hostsReady.get()) {
	    		allocationHandler.handleNewContainerAllocation(amResp);
	    	} else {
	    		monitor.update(amResp);
	    	}
	    }
	    return finish();
	}
	
	private boolean finish() throws IOException {
		comm.dispose();
		FinalApplicationStatus finishStatus  = null;
		String diagnostics = "";
		boolean isSuccess = true;
		if (monitor.getFailed() == 0) {
			finishStatus = FinalApplicationStatus.SUCCEEDED;
			LOG.info("Finished successfully.");
		} else {
			diagnostics = "Diagnostics:\n"
					+ "\t requested  = " + monitor.getRequested()+"\n"
					+ "\t completed  = " + monitor.getCompleted()+"\n"
					+ "\t allocated  = " + monitor.getAllocated()+"\n"
					+ "\t failed     = " + monitor.getFailed();
			
			finishStatus = FinalApplicationStatus.FAILED;
			LOG.info(diagnostics);
			isSuccess = false;
		}
		try {
			client.unregisterApplicationMaster(finishStatus, diagnostics, "");
		} catch (YarnException ex) {
			handleErronousExit(ex);
		}
		cleanTmp(conf);
		return isSuccess;
	}
	
	public Configuration getConfiguration() {
		return conf;
	}
	
	public Class<? extends Job> getJobClass() {
		return jobClass;
	}

	private static void cleanTmp(Configuration conf) {
		try {
			FileSystem fs = FileSystem.get(conf);
			Path tmpPath = new Path(conf.get(Constants.NEWT_JOB_TMP_DIR));
			if (fs.exists(tmpPath))fs.deleteOnExit(tmpPath);
			fs.close();
		} catch (IOException e) {
			LOG.error(e);
			System.exit(-1);
		}
	}

}
