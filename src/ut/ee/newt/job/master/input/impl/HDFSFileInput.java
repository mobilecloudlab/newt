/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.job.master.input.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Logger;

import ut.ee.newt.job.master.input.Input;

public class HDFSFileInput implements Input {
	
	private static final Logger LOG = Logger.getLogger(HDFSFileInput.class);

	private Path path;

	private Configuration conf;
	
	List<FileStatus> inputFiles;
	
	public HDFSFileInput(Configuration conf, String path) {
		this.path = new Path(path);
		this.conf = conf;
	}
	
	@Override
	public int initialize() {
	    FileSystem fs;
	    inputFiles = new ArrayList<FileStatus>();
		try {
			fs = FileSystem.get(conf);
			path = fs.makeQualified(path);
		    for (FileStatus f : fs.listStatus(path)) {
		    	if (!fs.isDirectory(f.getPath())) inputFiles.add(f);
		    }
		} catch (IOException e) {
			LOG.error(e.getMessage());
			System.exit(-1);
		}
		
		return inputFiles.size();
	}

	@Override
	public Object getForProcess(int pid) {
		//XXX return path local to the process if available
		return inputFiles.get(pid-1).getPath().toString();
	}

}
