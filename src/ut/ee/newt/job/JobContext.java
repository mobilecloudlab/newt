/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.job;

import java.util.ArrayDeque;
import java.util.Queue;

import org.apache.commons.collections.BidiMap;
import org.apache.hadoop.conf.Configuration;

import ut.ee.newt.Constants;
import ut.ee.newt.comm.buf.MessageBuffer;

public class JobContext {
	
	private int pid, numTasks;
	
	private Configuration conf;

	public BidiMap hosts;
	
	public ArrayDeque<BSPMessage>[] sendQueue, userQueue;
	public Queue<BSPMessage> recvQueue;
	
	public MessageBuffer[] recvBuf;
	public MessageBuffer[] sendBuf;
	
	protected BidiMap messageTypes;
	
	public int superstep;
	
	@SuppressWarnings("unchecked")
	JobContext(Configuration conf, int pid) {
		this.conf = conf;
		this.pid = pid;
		
		numTasks = conf.getInt(Constants.NEWT_NUM_TASKS, 0);
		
		userQueue = new ArrayDeque[numTasks];
		sendQueue = new ArrayDeque[numTasks];
		recvQueue = new ArrayDeque<BSPMessage>();
		for (int i = 0; i < numTasks; i++) {
			userQueue[i] = new ArrayDeque<BSPMessage>();
			sendQueue[i] = new ArrayDeque<BSPMessage>();
		}
		
		sendBuf = new MessageBuffer[numTasks];
		recvBuf = new MessageBuffer[numTasks];
		for (int i = 0; i < numTasks; i++) {
			if (i != getPid()) {
				sendBuf[i] = new MessageBuffer(this);
				recvBuf[i] = new MessageBuffer(this);
			}
		}
		superstep = 0;
	}
	
	public int getSuperstep() {
		return superstep;
	}
	
	public BidiMap getRegisteredMessageTypes() {
		return messageTypes;
	}
	
	public int getPid() {
		return pid;
	}
	
	public int getNumTasks() {
		return numTasks;
	}
	
	public Configuration getConf() {
		return conf;
	}
}
