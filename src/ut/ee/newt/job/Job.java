/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.job;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.apache.log4j.Logger;

import ut.ee.newt.comm.BSPComm;
import ut.ee.newt.comm.Communicator;
import ut.ee.newt.comm.Request;
import ut.ee.newt.misc.Packet;
import ut.ee.newt.misc.ProcessId;
import ut.ee.newt.misc.Utils;


public abstract class Job {
	
	public static interface InitializationHandler {

		public void handle(JobContext context, Communicator connector);
	}
	
	private static final Logger LOG = Logger.getLogger(Job.class);
	
	public List<InitializationHandler> initializationSequence;
	
	protected ProcessId pid;

	public Job(final ProcessId pid) {
		this.pid = pid;
		initializationSequence = new ArrayList<InitializationHandler>();
		initializationSequence.add(new InitializationHandler() {
			@Override
			public void handle(JobContext context, Communicator comm) {
				context.hosts = (DualHashBidiMap)
						comm.requestFromMasterSync(new Request(Packet.GET_HOSTS, pid.getId()));
				LOG.debug("Got hosts from master: "+context.hosts);
			}
		});
		initializationSequence.add(new InitializationHandler() {
			@Override
			public void handle(JobContext context, Communicator comm) {
				context.messageTypes = (DualHashBidiMap)
						comm.requestFromMasterSync(new Request(Packet.GET_MESSAGE_TYPES, pid.getId()));
				LOG.debug("Got messagetype map from master: "+context.messageTypes);
			}	
		});
	}
	
	abstract public void setup(JobContext context) throws Exception;
	
	abstract public void run(BSPComm bsp) throws Exception;
	
	abstract public void cleanup(JobContext context) throws Exception;
	
	@SuppressWarnings("unchecked")
	public static Job create(String className, ProcessId pid) {
		Job job = null;
		Class<? extends Job> jobClass;
		try {
			jobClass = (Class<? extends Job>) Class.forName(className);
			
			Constructor<?> constructor = Utils.getPossibleConstructor(jobClass, new Class[]{ProcessId.class});
			if (constructor == null) {
				LOG.error("No job constructor found");
				System.exit(-1);
			}
			
			Object[] parameterObjects = { pid };
			
			job = (Job) constructor.newInstance(parameterObjects);
		} catch (Exception e) {
			LOG.error(e.getMessage());
			System.exit(-1);
		}
		return job;
	}

}
