/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.job.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Logger;

import ut.ee.newt.Constants;
import ut.ee.newt.comm.BSPComm;
import ut.ee.newt.comm.Communicator;
import ut.ee.newt.comm.Request;
import ut.ee.newt.job.BSPState;
import ut.ee.newt.job.Checkpoint;
import ut.ee.newt.job.JobContext;
import ut.ee.newt.job.Stage;
import ut.ee.newt.misc.Packet;
import ut.ee.newt.misc.ProcessId;

public class FaultTolerantJob extends JobWithInput {
	
	private static final Logger LOG = Logger.getLogger(FaultTolerantJob.class);
	
	public static String STATE_CLASS = "state.class";
	private Class<? extends BSPState> stateClass;
	
	private BSPState state;
	
	private JobContext context;
	
	protected Map<String, Stage<BSPState>> stages;
	
	private boolean checkpointingEnabled, checkpointNeeded;
	private String checkpointDir;
	private String nextStage;
	private FileSystem fs;
	
	public FaultTolerantJob(final ProcessId pid) {
		super(pid);
		initializationSequence.add(new InitializationHandler() {
			@SuppressWarnings("unchecked")
			@Override
			public void handle(JobContext context, Communicator comm) {
				stages = (HashMap<String, Stage<BSPState>>)
						comm.requestFromMasterSync(new Request(Packet.GET_COMPUTATION_STAGES, pid.getId()));
				context.getConf().setClass(STATE_CLASS, (Class<?>)
						comm.requestFromMasterSync(new Request(Packet.GET_STATE_CLASS, pid.getId())),
						BSPState.class);
				context.getConf().set(Stage.INITIAL_STAGE, (String)
						comm.requestFromMasterSync(new Request(Packet.GET_INITIAL_STAGE, pid.getId())));
				context.getConf().set(Constants.NEWT_JOB_TMP_DIR, (String)
						comm.requestFromMasterSync(new Request(Packet.GET_CHECKPOINT_DIR, pid.getId())));
			}
		});
		
		checkpointingEnabled = true;
		checkpointNeeded = false;
	}
	
	@Override
	public void setup(JobContext context) {
		this.context = context;
		stateClass = context.getConf().getClass(STATE_CLASS, NullState.class, BSPState.class);
		checkpointDir = context.getConf().get(Constants.NEWT_JOB_TMP_DIR)+Path.SEPARATOR+"checkpoints";
		nextStage = context.getConf().get(Stage.INITIAL_STAGE);
		FSDataInputStream in = getInputStream();
		state = BSPState.create(stateClass, in, context);

		if (checkpointingEnabled) try {
			fs = FileSystem.get(context.getConf());
		} catch (IOException e1) {
			LOG.error("Failed to get filesystem implementation, checkpointing forcefully disabled");
			checkpointingEnabled = false;
		}
		
		try {
			if (in != null) in.close();
		} catch (IOException e) {
			LOG.error(e.getMessage());
		}
	}

	@Override
	public void run(BSPComm bsp) throws Exception {
		
		if (checkpointingEnabled) {
			if (pid.getAttemptId() > 0) {
				LOG.info("Starting process with recovery...");
				int target = bsp.sync();
				//XXX workaround for master not detecting failure in time
				if (target == 0) target = bsp.sync();
				LOG.info("Recovery target superstep is "+(target));
				nextStage = attemptRecovery(fs, checkpointDir, context, target);
				context.superstep--;
				bsp.sync();
			} else {
				checkpointNeeded = false;
			}
		}
		LOG.info("starting with '"+nextStage+"' (superstep "+context.getSuperstep()+")");
		String prev = nextStage;
		long bs = System.currentTimeMillis();
		nextStage = stages.get(nextStage).execute(bsp, state, context.superstep);
		//bsp.clearUserQueues();
		LOG.debug("computation time: "+(System.currentTimeMillis()-bs)+" ms.");
		LOG.debug("Done with '"+prev+"', next stage is '"+nextStage+"' (superstep "+context.getSuperstep()+")");
		
		while (true) {
			//long usedKB = (Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory())/1024;
			//LOG.info("Memory usage: "+usedKB+" kb. ");
			
			if (barrier(bsp, fs)) continue;
				
			if (nextStage.equals(Stage.STAGE_END)) break;
			if (checkpointingEnabled && checkpointNeeded) {
				attemptCheckpoint(fs, checkpointDir, context, nextStage);
			}
			
			//if (context.superstep == 25 && pid.getAttemptId() == 0 && bsp.pid() == 0) throw new Exception("halp");

			prev = nextStage;
			tryExecute(bsp);
			LOG.debug("Done with '"+prev+"', next stage is '"+nextStage+"' (superstep "+context.getSuperstep()+")");
		}
		
	}
	
	private boolean barrier(BSPComm bsp, FileSystem fs) throws Exception {
		long bs = System.currentTimeMillis();
		int status = bsp.sync();
		LOG.info("barrier: time = "+(System.currentTimeMillis()-bs)+" ms, status = "+status);
		
		if (status == -1) {
			checkpointNeeded = true;
		} else if (status > 0 && checkpointingEnabled) {
			LOG.info("Will attempt recovery from checkpoint at "+(status));
			nextStage = attemptRecovery(fs, checkpointDir, context, status);
			LOG.info("Restarting connections to peers...");
			bsp.restartPeerSessions();
			LOG.info("continuing with '"+nextStage+"' (superstep "+context.getSuperstep()+")");
			context.superstep--;
			return true;
		} else if (status != 0) {
			LOG.error("Barrier status is not 0, failing due to checkpointing not enabled");
			throw new Exception("Barrier status is not 0");
		}
		return false;
	}

	private void tryExecute(BSPComm bsp) {
		try {
			long bs = System.currentTimeMillis();
			nextStage = stages.get(nextStage).execute(bsp, state, context.superstep);
			//bsp.clearUserQueues();
			LOG.debug("computation time: "+(System.currentTimeMillis()-bs)+" ms.");
		} catch (NullPointerException ex) {
			if (!stages.containsKey(nextStage)) LOG.error("No stage named '"+nextStage+"'");
			else ex.printStackTrace();
			System.exit(-1);
		}
	}

	private void attemptCheckpoint(FileSystem fs, String checkpointDir, JobContext context, String nextStage) {
		try {
			Path newCheckpoint = fs.makeQualified(
					new Path(checkpointDir+Path.SEPARATOR+context.getPid()+"_"+context.getSuperstep()));
			FSDataOutputStream fsdos = fs.create(newCheckpoint);
			LOG.info("Storing checkpoint for superstep "+context.getSuperstep()+" to "+newCheckpoint);
			long bs = System.currentTimeMillis();
			Checkpoint.store(context, state, nextStage, fsdos);
			LOG.info("stored checkpoint in "+(System.currentTimeMillis()-bs)+" ms.");
			fsdos.close();
			checkpointNeeded = false;
		} catch (IOException e) {
			LOG.error(e.getMessage());
		}
	}
	
	private String attemptRecovery(FileSystem fs, String checkpointDir, JobContext context, int targetStep) {
		String next = Stage.STAGE_END;
		try {
			if (targetStep == 0) {
				setup(context);
				next = context.getConf().get(Stage.INITIAL_STAGE);
				context.superstep = targetStep;
			} else {
				Path possibleCheckpoint = fs.makeQualified(new Path(checkpointDir+Path.SEPARATOR+context.getPid()+"_"+targetStep));
				if (fs.exists(possibleCheckpoint)) {
					LOG.info("Found requested checkpoint "+possibleCheckpoint);
					Path newCheckpoint = fs.makeQualified(possibleCheckpoint);
					FSDataInputStream fsdis = fs.open(newCheckpoint);
					long bs = System.currentTimeMillis();
					next = Checkpoint.recover(targetStep, context, state, fsdis);
					LOG.info("recovered from checkpoint in "+(System.currentTimeMillis()-bs)+" ms.");
					fsdis.close();
				} else {
					LOG.error("Requested checkpoint not found!");
				}
			}
			checkpointNeeded = false;
		} catch (IOException e) {
			LOG.error(e.getMessage());
		}
		return next;
	}

	@Override
	public void cleanup(JobContext context) {
		
	}

}
