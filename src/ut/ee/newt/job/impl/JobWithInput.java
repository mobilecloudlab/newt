/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.job.impl;

import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.log4j.Logger;

import ut.ee.newt.comm.Communicator;
import ut.ee.newt.comm.Request;
import ut.ee.newt.job.Job;
import ut.ee.newt.job.JobContext;
import ut.ee.newt.misc.Packet;
import ut.ee.newt.misc.ProcessId;

public abstract class JobWithInput extends Job {
	
	private static final Logger LOG = Logger.getLogger(JobWithInput.class);

	Path inputPath;
	
	private JobContext contxt;
	
	public JobWithInput(final ProcessId pid) {
		super(pid);
		initializationSequence.add(new InitializationHandler() {
			@Override
			public void handle(JobContext context, Communicator comm) {
				String p = (String)
						comm.requestFromMasterSync(new Request(Packet.GET_INPUT, pid.getId()));
				inputPath = !p.isEmpty() ? new Path(p) : null;
				contxt = context;
			}
		});
	}
	
	public FSDataInputStream getInputStream() {
		FileSystem fs;
		FSDataInputStream fsdis = null;
		if (inputPath != null) try {
			fs = FileSystem.get(contxt.getConf());
			fsdis = fs.open(inputPath);
		} catch (Exception e) {
			LOG.error(e.getMessage());
		}
		return fsdis;
	}
}
