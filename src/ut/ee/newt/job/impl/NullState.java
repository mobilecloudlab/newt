package ut.ee.newt.job.impl;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import ut.ee.newt.job.BSPState;
import ut.ee.newt.job.JobContext;

public class NullState extends BSPState {


	public NullState(JobContext context) {
		super(context);
	}

	@Override
	public void write(DataOutput out) throws IOException {
		
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		
	}

}
