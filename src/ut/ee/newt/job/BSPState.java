/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.job;

import java.io.DataInput;
import java.lang.reflect.Constructor;

import org.apache.hadoop.io.Writable;
import org.apache.log4j.Logger;

import ut.ee.newt.misc.Utils;

public abstract class BSPState implements Writable {
	
	private static final Logger LOG = Logger.getLogger(BSPState.class);
	
	public BSPState(JobContext context) {
		
	}
	
	public static BSPState create(Class<?> stateClass, DataInput in, JobContext context) {
		BSPState newInstance = null;
		Constructor<?> constructor = Utils.getPossibleConstructor(stateClass, new Class[]{JobContext.class});
		if (constructor == null) {
			LOG.error("No state constructor found");
			System.exit(-1);
		}
		
		Object[] parameterObjects = { context };
		try {
			if (in != null) {
				newInstance = (BSPState) constructor.newInstance(parameterObjects);
				newInstance.readFields(in);
			} else {
				newInstance = (BSPState) constructor.newInstance(parameterObjects);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			LOG.error("Could not instantiate "+stateClass+", reason: "+ex.getMessage());
			System.exit(-1);
		}
		return newInstance;
	}
}
