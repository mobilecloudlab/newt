/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.job;

import java.lang.Thread.UncaughtExceptionHandler;

import org.apache.hadoop.conf.Configuration;

import ut.ee.newt.Constants;
import ut.ee.newt.NEWTConfiguration;
import ut.ee.newt.comm.BSPComm;
import ut.ee.newt.comm.Communicator;
import ut.ee.newt.comm.mina.MINACommunicator;
import ut.ee.newt.misc.ProcessId;

public class JobRunner {
	
	public static void main(String[] args) {
		int status = 0;
		Thread.setDefaultUncaughtExceptionHandler(new UncaughtExceptionHandler(){
			@Override
			public void uncaughtException(Thread t, Throwable ex) {
				ex.printStackTrace();
				System.exit(-1);
			}
		});
		
		Configuration conf = new Configuration(NEWTConfiguration.createDefault());
		Job job = Job.create(args[0], ProcessId.fromIdString(args[5]));
		
		int pid = Integer.parseInt(args[1]);
		conf.setInt(Constants.NEWT_NUM_TASKS, Integer.parseInt(args[2]));
		JobContext context = new JobContext(conf, pid);
		
		Communicator comm = MINACommunicator.create(context, args[3], Integer.parseInt(args[4]));
		
		for (Job.InitializationHandler init: job.initializationSequence) {
			init.handle(context, comm);
		}
		
		try {
			job.setup(context);
			
			BSPComm bsp = new BSPComm(context, comm);
			
			job.run(bsp);
			job.cleanup(context);
		} catch (Exception ex) {
			ex.printStackTrace();
			status = -1;
		}
		
		comm.dispose();
		System.exit(status);
	}

}
