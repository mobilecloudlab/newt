/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.job;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayDeque;

import org.apache.hadoop.io.Writable;
import org.apache.log4j.Logger;

import ut.ee.newt.job.impl.FaultTolerantJob;
import ut.ee.newt.job.impl.NullState;

public class Checkpoint<S extends BSPState> implements Writable {
	
	private static final Logger LOG = Logger.getLogger(Checkpoint.class);
	
	int superstep;
	
	S state;
	
	//private Class<S> stateClass;
	
	String nextStage;
	
	JobContext context;
	
	
	private Checkpoint(S state, int superstep, JobContext context) {
		this.context = context;
		this.state = state;
		this.superstep = superstep;
	}

	private Checkpoint(S state, int superstep, Class<S> stateClass, JobContext context) {
		this(state, superstep, context);
		//this.stateClass = stateClass;
	}
	
	private Checkpoint(S state, int superstep, String nextStage, JobContext context) {
		this(state, superstep, context);
		this.nextStage = nextStage;
	}
	
	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(superstep);
		out.writeUTF(nextStage);
		state.write(out);
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		int readSuperstep = in.readInt();
		if (readSuperstep != superstep)
			LOG.warn("Loading checkpoint with unexpected superstep!");
		nextStage = in.readUTF();
		state.readFields(in);
		//state = stateClass.cast(StateFactory.create(stateClass, in, context));
	}
	
	@SuppressWarnings("unchecked")
	public static <S extends BSPState> String recover(int superstep, JobContext context, S state, DataInput in) {
		Class<S> stateClass = (Class<S>) context.getConf().getClass(FaultTolerantJob.STATE_CLASS, NullState.class, BSPState.class);
		Checkpoint<S> recovered = new Checkpoint<S>(state, superstep, stateClass, context);
		try {
			recovered.readFields(in);
			
			int msgCount = in.readInt();
			for (ArrayDeque<BSPMessage> q : context.userQueue) {
				q.clear();
			}
			for (int i = 0; i < msgCount; i++) {
				int source = in.readInt();
				Class<? extends Writable> type = (Class<? extends Writable>) Class.forName(in.readUTF());
				BSPMessage msg = BSPMessage.create(in, source, context.getPid(), recovered.superstep, type);
				context.userQueue[source].push(msg);
			}
			context.superstep = superstep;
		} catch (Exception e) {
			LOG.error(e.getMessage());
			e.printStackTrace();
		}
		return recovered.nextStage;
	}
	
	public static <S extends BSPState> boolean store(JobContext context, S state, String nextStage, DataOutput out) {
		Checkpoint<S> newCheckpoint = new Checkpoint<S>(state, context.getSuperstep(), nextStage, context);
		boolean success = true;
		try {
			newCheckpoint.write(out);
			
			int msgCount = 0;
			for (ArrayDeque<BSPMessage> q : context.userQueue) {
				msgCount += q.size();
			}
			out.writeInt(msgCount);
			for (ArrayDeque<BSPMessage> q : context.userQueue) {
				for (BSPMessage msg : q) {
					out.writeInt(msg.source);
					out.writeUTF(msg.getMessageType().getName());
					msg.write(out);
				}
			}
			
		} catch (IOException e) {
			LOG.error(e.getMessage());
			e.printStackTrace();
			success = false;
		}
		return success;
	}

}
