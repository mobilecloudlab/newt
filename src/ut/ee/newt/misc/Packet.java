/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.misc;

public enum Packet {
	GET_HOSTS,
	PING,
	PONG,
	EXIT_BARRIER,
	BUFFERED_MESSAGES,
	ENTERED_BARRIER,
	GET_MESSAGE_TYPES,
	EXITED_BARRIER,
	GET_COMPUTATION_STAGES,
	GET_INITIAL_STAGE,
	GET_INPUT,
	NULL_RESPONSE,
	GET_STATE_CLASS,
	GET_CHECKPOINT_DIR,
	RECOVERY_NEEDED,
	CHECKPOINT_NEEDED,
	GET_RECOVERY_NEEDED,
	GET_CURRENT_SUPERSTEP
}
