/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.misc;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.yarn.api.records.LocalResource;
import org.apache.hadoop.yarn.api.records.LocalResourceType;
import org.apache.hadoop.yarn.api.records.LocalResourceVisibility;
import org.apache.hadoop.yarn.util.ConverterUtils;
import org.apache.log4j.Logger;

public class LocalResources {
	
	private static final Logger LOG = Logger.getLogger(LocalResources.class);

	Map<String, LocalResource> localResources;
	
	FileSystem fs;
	
	public LocalResources(FileSystem fs) {
		this.fs = fs;
		localResources = new HashMap<String, LocalResource>();
	}
	
	public void add(String path, String localPath) throws IOException {
		Path resPath = fs.makeQualified(new Path(path));

		FileStatus resStatus = fs.getFileStatus(resPath);
		LOG.debug("Localazing: "+resPath+" to "+localPath);
		localResources.put(localPath, createLocalResource(resStatus, LocalResourceType.FILE));
	}
	
	public void addArchive(String path, String localPath) throws IOException {
		Path resPath = fs.makeQualified(new Path(path));

		FileStatus resStatus = fs.getFileStatus(resPath);
		LOG.debug("Localazing: "+resPath+" to "+localPath);
		localResources.put(localPath, createLocalResource(resStatus, LocalResourceType.ARCHIVE));
	}
	
	public void addAll(String path, String localPath) throws IOException {
		Path resPath = fs.makeQualified(new Path(path));
		int start = resPath.toString().length()+1;
		RemoteIterator<LocatedFileStatus> it = fs.listFiles(resPath, true);
		while (it.hasNext()) {
			LocatedFileStatus next = it.next();
			String p = next.getPath().toString();
			String local = localPath+p.substring(start);
			LOG.debug("Localazing: "+p+" to "+local);
			localResources.put(local, createLocalResource(next, LocalResourceType.FILE));
		}
	}
	
	public Map<String, LocalResource> get() {
		return localResources;
	}
	
	LocalResource createLocalResource(FileStatus resStatus, LocalResourceType type) {
		LocalResource rsrc = LocalResource.newInstance(
				ConverterUtils.getYarnUrlFromPath(resStatus.getPath()),
				type, LocalResourceVisibility.APPLICATION, 
				resStatus.getLen(), 
				resStatus.getModificationTime());
		return rsrc;
	}
}
