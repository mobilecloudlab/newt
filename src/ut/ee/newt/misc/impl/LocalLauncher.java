/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.misc.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.yarn.api.records.ContainerLaunchContext;
import org.apache.log4j.Logger;

import ut.ee.newt.misc.Launcher;
import ut.ee.newt.misc.LocalResources;

public class LocalLauncher extends Launcher {

	private static final Logger LOG = Logger.getLogger(LocalLauncher.class);

	private Configuration conf;
	
	public LocalLauncher(Configuration conf) {
		this.conf = conf;
	}

	@Override
	public void setup(ContainerLaunchContext context, Class<?> jobClass, String[] args, int memory) {
		LocalResources localResources = null;
		try {
			localResources = new LocalResources(FileSystem.getLocal(conf));
			String path = jobClass.getResource(jobClass.getSimpleName()+".class").getPath();
			if (path.startsWith("file:")) {
				String jarPath = path.substring(5,path.indexOf('!'));
				String local = jarPath.substring(jarPath.lastIndexOf(Path.SEPARATOR_CHAR)+1);
				localResources.addArchive(jarPath, local);
			} else {
				String className = jobClass.getName();
				path = path.substring(0, path.indexOf(className.substring(0,className.indexOf('.'))));
				localResources.addAll(path, "");
			}
		} catch (IOException e) {
			LOG.error(e);
			System.exit(-1);
		}
		context.setLocalResources(localResources.get());
		
		String command = "${JAVA_HOME}/bin/java -Xmx"+memory+"m -Xms"+memory+"m -cp ./:${CLASSPATH} " + jobClass.getName();
		for (String arg : args)
			command += " "+arg;
		command += " 1> "+stdOut+" 2> "+stdErr;

		List<String> commands = new ArrayList<String>();
		commands.add(command);
		context.setCommands(commands);
	}

}
