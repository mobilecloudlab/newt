/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.misc.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.yarn.api.records.ContainerLaunchContext;
import org.apache.log4j.Logger;

import ut.ee.newt.Constants;
import ut.ee.newt.misc.Launcher;
import ut.ee.newt.misc.LocalResources;

public class RemoteLauncher extends Launcher {

	private static final Logger LOG = Logger.getLogger(RemoteLauncher.class);

	private Configuration conf;

	FileSystem dfs;
	
	private String tmpDir;
	
	public RemoteLauncher(Configuration conf) {
		this.conf = conf;
		tmpDir = conf.get(Constants.NEWT_JOB_TMP_DIR);
	}

	@Override
	public void setup(ContainerLaunchContext context, Class<?> jobClass, String[] args, int memory) {
		LocalResources localResources = null;
		try {
			dfs = FileSystem.get(conf);
		} catch (IOException e) {
			LOG.error(e);
			System.exit(-1);
		}
		Path tmpPath = dfs.makeQualified(new Path(tmpDir+Path.SEPARATOR+"jar"));
		
		try {
			synchronized (conf) {
				if (!dfs.exists(tmpPath)) {
					dfs.mkdirs(tmpPath);
					LOG.debug("Storing temporarily in "+tmpPath);
					String path = jobClass.getResource(jobClass.getSimpleName()+".class").getPath();
					if (path.startsWith("file:")) {
						localizeFile(tmpPath, path);
					} else {
						localizeJar(tmpPath, path, jobClass);
					}
				}
			}
			localResources = new LocalResources(dfs);
			LOG.debug("Localizing contents of '"+tmpPath+"'");
			localResources.addAll(tmpPath.toString(), "");
		} catch (IOException e) {
			LOG.error(e);
			e.printStackTrace();
			System.exit(-1);
		}
		context.setLocalResources(localResources.get());
		
		String command = "${JAVA_HOME}/bin/java -Xmx"+memory+"m -Xms"+memory+"m -cp ./jar/*:${CLASSPATH} " + jobClass.getName();
		for (String arg : args)
			command += " "+arg;
		command += " 1> "+stdOut+" 2> "+stdErr;

		List<String> commands = new ArrayList<String>();
		commands.add(command);
		context.setCommands(commands);
		
	}

	private void localizeJar(Path targetDir, String path, Class<?> jobClass) throws FileNotFoundException, IOException {
		String className = jobClass.getName();
		path = path.substring(0, path.indexOf(className.substring(0,className.indexOf('.'))));
		int start = path.length()+1;
		RemoteIterator<LocatedFileStatus> it = FileSystem.getLocal(conf).listFiles(new Path(path), true);
		while (it.hasNext()) {
			LocatedFileStatus next = it.next();
			String p = next.getPath().toString();
			String local = path+p.substring(start);
			LOG.debug("Localazing: "+p+" to "+local);
			dfs.copyFromLocalFile(next.getPath(), new Path(targetDir.getName()+Path.SEPARATOR_CHAR+local));
		}
	}

	private void localizeFile(Path targetDir, String filePath) throws IOException {
		String jarPath = filePath.substring(5,filePath.indexOf('!'));
		String local = jarPath.substring(jarPath.lastIndexOf(Path.SEPARATOR_CHAR)+1);
		Path src = new Path(jarPath);
		Path tgt = new Path(targetDir.toString()+Path.SEPARATOR_CHAR+local);
		if (!dfs.exists(tgt)) {
			LOG.debug("Copying "+src+" to "+tgt);
			dfs.copyFromLocalFile(src, tgt);
		}
	}
}
