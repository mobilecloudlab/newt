/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.misc;

import org.apache.hadoop.yarn.api.records.ContainerLaunchContext;

public abstract class Launcher {

	protected String stdOut = "/dev/null", stdErr = "/dev/null";
	
	public abstract void setup(ContainerLaunchContext context, Class<?> jobClass, String[] args, int memory);
	
	public void setOutLogPath(String path) {
		stdOut = path;
	}
	
	public void setErrLogPath(String path) {
		stdErr = path;
	}
}
