/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.misc;

import org.apache.hadoop.yarn.api.records.ContainerId;

public class ProcessId {
	
	private ContainerId containerId;
	
	private int attemptId;

	private int processId;
	
	public ProcessId(ContainerId containerId, int pid) {
		this.containerId = containerId;
		processId = pid;
		attemptId = 0;
	}
	
	public ContainerId getContainerId() {
		return containerId;
	}

	public void setContainerId(ContainerId containerId) {
		this.containerId = containerId;
	}

	public int getId() {
		return processId;
	}

	public void setPid(int processId) {
		this.processId = processId;
	}
	
	@Override
	public String toString() {
		return "pid-"+processId+"-"+attemptId+"-"+containerId;
	}

	public int getAttemptId() {
		return attemptId;
	}

	public void setAttemptId(int attemptId) {
		this.attemptId = attemptId;
	}

	public static ProcessId fromIdString(String string) {
		String[] symbols = string.split("-");
		ProcessId newPid = new ProcessId(null,Integer.parseInt(symbols[1]));
		newPid.setAttemptId(Integer.parseInt(symbols[2]));
		return newPid;
	}
}
