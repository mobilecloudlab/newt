/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.misc;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import org.apache.hadoop.security.UserGroupInformation;
import org.apache.hadoop.yarn.api.records.ApplicationAccessType;
import org.apache.hadoop.yarn.api.records.ContainerLaunchContext;
import org.apache.hadoop.yarn.util.Records;
import org.apache.log4j.Logger;

abstract public class Utils {
	
	private static final Logger LOG = Logger.getLogger(Utils.class);
	
	public static Constructor<?> getPossibleConstructor(Class<?> motherClass, Class<?>[] pars) {
		Constructor<?>[] ctors = motherClass.getConstructors();
	
		for (Constructor<?> c : ctors) {
		Class<?>[] cpars = c.getParameterTypes();
	
		if (cpars.length != pars.length) continue;
			boolean found = true;
			for (int i = 0; i < cpars.length; ++i) {
				if (!cpars[i].isAssignableFrom(pars[i])) {
					found = false;
					break;
				}
			}
			if (found)
				return c;
		}
		return null;
	}
	
	static public ContainerLaunchContext createContainerLaunchContext() {
		ContainerLaunchContext context = Records.newRecord(ContainerLaunchContext.class);

		String owner = null;
		try {
			owner = UserGroupInformation.getCurrentUser().getShortUserName();
		} catch (IOException e) {
			LOG.error(e.getMessage());
			System.exit(-1);
		}
		Map<ApplicationAccessType, String> acls = new HashMap<ApplicationAccessType, String>();
		acls.put(ApplicationAccessType.VIEW_APP, owner);
		acls.put(ApplicationAccessType.MODIFY_APP, owner);
		context.setApplicationACLs(acls);
		
		Map<String, String> env = new HashMap<String, String>();
		String classPathEnv = System.getProperty("java.class.path") + ":./*";
		//System.out.println("CLASSPATH: " + classPathEnv);
		env.put("CLASSPATH", classPathEnv);
		context.setEnvironment(env);
		
		return context;
	}
}
