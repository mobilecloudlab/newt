/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.yarn.api.records.ApplicationId;
import org.apache.hadoop.yarn.api.records.ApplicationReport;
import org.apache.hadoop.yarn.api.records.ContainerLaunchContext;
import org.apache.hadoop.yarn.api.records.Priority;
import org.apache.hadoop.yarn.api.records.Resource;
import org.apache.hadoop.yarn.api.records.YarnApplicationState;
import org.apache.hadoop.yarn.client.api.YarnClient;
import org.apache.hadoop.yarn.client.api.YarnClientApplication;
import org.apache.hadoop.yarn.client.api.impl.YarnClientImpl;
import org.apache.hadoop.yarn.exceptions.YarnException;
import org.apache.log4j.Logger;

import ut.ee.newt.misc.Launcher;
import ut.ee.newt.misc.Utils;
import ut.ee.newt.misc.impl.RemoteLauncher;

public class NEWTClient {

	private static final Logger LOG = Logger.getLogger(NEWTClient.class);
	
	private ApplicationId appId;
	private String appName = "newt";
	
	Launcher launcher;
	
	YarnClient client;
	
	Configuration conf;
	
	public NEWTClient(Configuration conf) {
		client = new YarnClientImpl();
		this.conf = conf;
		conf.set(Constants.NEWT_LOCAL_LOG_DIR, System.getProperty("user.dir"));
		conf.set(Constants.NEWT_JOB_TMP_DIR, "/user/"+System.getProperty("user.name")+"/tmp");
		launcher = new RemoteLauncher(conf);
		client.init(conf);
	}

	public NEWTClient() {
		this(NEWTConfiguration.createDefault());
	}

	public void submit(Class<?> jobClass, String[] args, boolean waitForCompletion) {
		cleanTmp(conf);
		String className = jobClass.getSimpleName()+".class";
		LOG.info("Submitting "+className);
		try {
			submitApplication(jobClass, args);
			if (waitForCompletion)
				monitorSubmittedApp();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (YarnException e) {
			e.printStackTrace();
		} finally {
			cleanTmp(conf);
		}
	}
	
	synchronized private void submitApplication(Class<?> jobClass, String[] args) throws InterruptedException, IOException {
		client.start();
		YarnClientApplication application = null;
		try {
			application = client.createApplication();
		} catch (YarnException e) {
			LOG.error(e.getMessage());
			System.exit(-1);
		}
		appId = application.getNewApplicationResponse().getApplicationId();
		LOG.debug("Got new ApplicationId=" + appId);

		int memory = conf.getInt(Constants.NEWT_MASTER_MEMORY, 256);
		ContainerLaunchContext amContainer = Utils.createContainerLaunchContext();
		String path = conf.get(Constants.NEWT_LOCAL_LOG_DIR);
		LOG.info("Logging to '"+path+"'");
		launcher.setErrLogPath(path+"/log-master");
		launcher.setup(amContainer, jobClass, args, memory);
		
		application.getApplicationSubmissionContext().setAMContainerSpec(amContainer);
		application.getApplicationSubmissionContext().setApplicationName(appName);
		application.getApplicationSubmissionContext().setResource(Resource.newInstance(memory, 1));
		application.getApplicationSubmissionContext().setPriority(Priority.newInstance(0));
		
		try {
			client.submitApplication(application.getApplicationSubmissionContext());
		} catch (YarnException e) {
			LOG.error(e.getMessage());
			cleanTmp(conf);
			System.exit(-1);
		}
	}
	
	public void close() {
		try {
			client.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	synchronized private void monitorSubmittedApp() throws InterruptedException, IOException, YarnException {
		YarnApplicationState oldState = YarnApplicationState.NEW;
		boolean wasAccepted = false;
	    while (true) {
		    
	        ApplicationReport report = null;
			try {
				report = client.getApplicationReport(appId);
			} catch (YarnException e) {
				LOG.error(e.getMessage());
			}
	        YarnApplicationState state = report.getYarnApplicationState();
	        if (oldState.compareTo(state) != 0)
	        	LOG.info("Job status update: "+state);
	        oldState = state;
	        if (!wasAccepted && state.compareTo(YarnApplicationState.RUNNING) == 0)
	        	wasAccepted = true;
	        if (wasAccepted && state.compareTo(YarnApplicationState.ACCEPTED) == 0) {
	        	LOG.warn("ApplicationMaster failed and was restarted by YARN, killing it for good");
	        	client.killApplication(appId);
	        }
	        if (state.compareTo(YarnApplicationState.FINISHED) == 0 ||
	        		state.compareTo(YarnApplicationState.KILLED) == 0 ||
	        		state.compareTo(YarnApplicationState.FAILED) == 0) {
	        	LOG.debug(report.getDiagnostics());
	        	LOG.info("Application finished in "+(report.getFinishTime()-report.getStartTime())/1000.0
	    	    		+" s. with: "+state+"/"+report.getFinalApplicationStatus());
	    	    break;
	        }
	        wait(200);
	    }
	}
	
	private static void cleanTmp(Configuration conf) {
		try {
			FileSystem fs = FileSystem.get(conf);
			Path tmpPath = new Path(conf.get(Constants.NEWT_JOB_TMP_DIR));
			if (fs.exists(tmpPath))fs.deleteOnExit(tmpPath);
			fs.close();
		} catch (IOException e) {
			LOG.error(e);
			System.exit(-1);
		}
	}
}
