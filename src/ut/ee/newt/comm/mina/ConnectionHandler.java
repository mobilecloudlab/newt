/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.comm.mina;

import org.apache.log4j.Logger;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;

import ut.ee.newt.job.BSPMessage;
import ut.ee.newt.job.JobContext;
import ut.ee.newt.misc.Packet;

public class ConnectionHandler extends IoHandlerAdapter {
	
	JobContext context;
	
	public ConnectionHandler(JobContext context) {
		this.context = context;
	}
	
	private static Logger LOG = Logger.getLogger(ConnectionHandler.class);
    
    @Override
    public void messageReceived(IoSession session, Object message) throws Exception {
    	if (message instanceof Packet) {
    		Packet request = (Packet) message;
    		switch (request) {
        	case PING:
        		session.write(Packet.PONG);
                break;
            default:
            	break;
        	}
    	} else if (message instanceof BSPMessage) {
    		BSPMessage msg = (BSPMessage) message;
    		synchronized (context.recvQueue) {
    			context.recvQueue.add(msg);
    		}
    	}
    }
    
    @Override
    public void sessionIdle(IoSession session, IdleStatus status) throws Exception {
        LOG.info("IDLE " + session.getIdleCount(status));
    }
	
	@Override
    public void exceptionCaught(IoSession session, Throwable e) throws Exception {
        LOG.error(e.getMessage());
        e.printStackTrace();
        session.close(true);
    }
}
