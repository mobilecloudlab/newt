/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.comm.mina;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.log4j.Logger;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;

import ut.ee.newt.Constants;
import ut.ee.newt.comm.Request;
import ut.ee.newt.job.master.ApplicationMaster;
import ut.ee.newt.job.master.ApplicationMaster.RequestResponse;
import ut.ee.newt.misc.Packet;

public class MasterConnectionHandler extends IoHandlerAdapter {
	
	private static Logger LOG = Logger.getLogger(MasterConnectionHandler.class);
	
	IoSession[] sessions;
	
	AtomicInteger sessionsInBarrier;
	
	ApplicationMaster am;
	
	List<Integer> checkpoints;
	
	long lastCP, checkpointInterval;
	
	public MasterConnectionHandler(ApplicationMaster am) {
		this.am = am;
		
		checkpoints = new ArrayList<Integer>();
		//add a base "checkpoint" as the reference for restarting
		checkpoints.add(-1);
		
		sessionsInBarrier = new AtomicInteger();
		sessions = new IoSession[am.getConfiguration().getInt(Constants.NEWT_NUM_TASKS, 0)];
		
		checkpointInterval = 60000;
	}
	
	@Override
    public void exceptionCaught(IoSession session, Throwable e) throws Exception {
        am.handleErronousExit(e);
    }
	
    @Override
    public void messageReceived(IoSession session, Object message) throws Exception {
    	if (message instanceof Request) {
    		Request request = (Request) message;
    		switch (request.type) {
        	case EXIT_BARRIER:
        		handleBarrier(session);
        		break;
        	case GET_HOSTS:
        		ensureAllContainersReady();
        		//keep a handle to the session for barrier synchronization
        		synchronized (sessions) {
        			sessions[request.source] = session;
        		}
                session.write(am.getHosts());
				break;
            default:
        		RequestResponse reply = am.getRequestHandlers().get(request.type);
        		if (reply == null) {
        			LOG.warn("The request of type "+request.type+" has no RequestResponse associated with it!");
        			//should cause a cast exception on the remote process, failing it
        			session.write(Packet.NULL_RESPONSE);
        		} else {
        			session.write(reply.respond(request.source));
        		}
            	break;
        	}
    	}
    }
    
    private void handleBarrier(IoSession session) throws InterruptedException {
    	LOG.debug(session.getRemoteAddress()+" in barrier at superstep "+am.superstep);
    	if (sessionsInBarrier.incrementAndGet() == sessions.length) {
			
			ensureAllContainersReady();
			
			if (am.superstep.get() == 0)
				lastCP = System.currentTimeMillis();
			
			boolean checkpointNeeded = false;
    		if (am.barrierExitStatus != Packet.RECOVERY_NEEDED &&
    				System.currentTimeMillis()-lastCP > checkpointInterval &&
    				am.superstep.get() != checkpoints.get(checkpoints.size()-1)) {
				LOG.info("Requesting checkpoints at superstep: "+am.superstep.get());
				checkpointNeeded = true;
				checkpoints.add(am.superstep.get());
				lastCP = System.currentTimeMillis();
			}
			LOG.debug("Broadcasting barrier exit status: "+am.barrierExitStatus+" (superstep "+am.superstep.get()+")");
			for (IoSession s : sessions) {
				if (checkpointNeeded) {
					s.write(Packet.CHECKPOINT_NEEDED);
				} else {
					s.write(am.barrierExitStatus);
    				if (am.barrierExitStatus == Packet.RECOVERY_NEEDED) {
    					int target = checkpoints.get(checkpoints.size()-1);
    					s.write(target+1);
    				}
				}
			}
			if (am.barrierExitStatus == Packet.RECOVERY_NEEDED) {
				am.superstep.set(checkpoints.get(checkpoints.size()-1));
				LOG.info("Recovery request for superstep "+am.superstep.get()+" sent");
				am.barrierExitStatus = Packet.EXITED_BARRIER;
				lastCP = System.currentTimeMillis();
			}
			am.superstep.incrementAndGet();
			sessionsInBarrier.addAndGet(-sessions.length);
		}
	}

	private void ensureAllContainersReady() throws InterruptedException {
		synchronized(this) {
			while (!am.hostsReady.get()) wait(1);
		}
	}

	@Override
    public void sessionIdle(IoSession session, IdleStatus status) throws Exception {
        LOG.info("IDLE " + session.getIdleCount(status));
    }

}
