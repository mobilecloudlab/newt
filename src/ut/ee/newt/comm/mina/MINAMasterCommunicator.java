/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.comm.mina;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.apache.hadoop.conf.Configuration;
import org.apache.log4j.Logger;
import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.serialization.ObjectSerializationCodecFactory;
import org.apache.mina.transport.socket.SocketSessionConfig;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

import ut.ee.newt.Constants;
import ut.ee.newt.comm.MasterCommunicator;
import ut.ee.newt.job.master.ApplicationMaster;

public class MINAMasterCommunicator implements MasterCommunicator {

	private static final Logger LOG = Logger.getLogger(MINAMasterCommunicator.class);
	
	private IoAcceptor acceptor;
	
	ApplicationMaster master;
	
	private MINAMasterCommunicator(ApplicationMaster master) {
		this.master = master;
	}
	
	public static MINAMasterCommunicator create(ApplicationMaster master) {
		MINAMasterCommunicator comm = new MINAMasterCommunicator(master);
		return comm;
	}
	
	private IoAcceptor initConnectionAcceptor(int port) {
		IoAcceptor acceptor = new NioSocketAcceptor();
	    //acceptor.getFilterChain().addLast("logger", new LoggingFilter());
	    acceptor.getFilterChain().addLast("codec", new ProtocolCodecFilter(new ObjectSerializationCodecFactory()));
	    acceptor.setHandler(new MasterConnectionHandler(master));
        acceptor.getSessionConfig().setReadBufferSize(256);
        ((SocketSessionConfig)acceptor.getSessionConfig()).setTcpNoDelay(true);
        
        try {
			acceptor.bind(new InetSocketAddress(port));
		} catch (IOException e) {
			LOG.error("Failed to bind server to port: "+port+"("+e.getMessage()+")");
			master.handleErronousExit(e);
		}
        return acceptor;
	}

	@Override
	public void init(Configuration conf) {
		acceptor = initConnectionAcceptor(conf.getInt(Constants.NEWT_MASTER_PORT, 12999));
	}
	

	@Override
	public void dispose() {
		acceptor.dispose();
	}
	
	
}
