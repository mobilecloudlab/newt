/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.comm.mina;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.log4j.Logger;

import ut.ee.newt.job.JobContext;
import ut.ee.newt.misc.Packet;

public class ProcessConnector extends IoHandlerAdapter {
	
	private static Logger LOG = Logger.getLogger(ProcessConnector.class);
	
	@SuppressWarnings("unused")
	private JobContext context;
	
	private int target;
	
	private long pingTime;
	
	public ProcessConnector(JobContext context, int target) {
		this.context = context;
		this.target = target;
	}
    
    @Override
    public void sessionCreated(IoSession session) {
    	session.setAttribute("process_id", target);
    }

    @Override
    public void sessionOpened(IoSession session) {
    	//session.write(Packet.PING);
    	pingTime = System.currentTimeMillis();
    }

    @Override
    public void messageReceived(IoSession session, Object message) {
    	if (message instanceof Packet) {
    		Packet request = (Packet) message;
    		switch (request) {
        	case PONG:
        		LOG.info("Ping to "+target+": "+(System.currentTimeMillis()-pingTime)+" ms.");
                break;
            default:
            	break;
        	}
    	}
    }
    
    @Override
    public void exceptionCaught(IoSession session, Throwable e) {
        LOG.error(e.getMessage());
        e.printStackTrace();
    	session.close(true);
    }
}
