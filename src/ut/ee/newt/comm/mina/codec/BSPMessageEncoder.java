/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.comm.mina.codec;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

import ut.ee.newt.comm.buf.MessageBuffer;
import ut.ee.newt.job.JobContext;
import ut.ee.newt.misc.Packet;

public class BSPMessageEncoder implements ProtocolEncoder {
    
	JobContext context;
	
	MessageBuffer sendBuffer;

	int target;
	
	public BSPMessageEncoder(JobContext context, int target) {
		this.context = context;
		this.sendBuffer = context.sendBuf[target-1];
		this.target = target;
	}

	@Override
	public void encode(IoSession session, Object message, ProtocolEncoderOutput out) throws Exception {
		if (message instanceof Packet) {
			Packet msg = (Packet) message;
			IoBuffer buf2 = IoBuffer.allocate(9);
	    	buf2.setAutoExpand(true);
	    	buf2.putEnum(msg);
	    	buf2.putInt(context.getPid());
	    	buf2.putInt(context.getSuperstep());
	    	buf2.flip();
	        out.write(buf2);
			switch (msg) {
			case BUFFERED_MESSAGES:
				out.write(sendBuffer.getPrefix());
				IoBuffer buf = sendBuffer.get();
		        if (!sendBuffer.isEmpty()) {
		        	out.write(buf);
		        }
		        break;
		    default:
		    	break;
			}
		}
		out.mergeAll();
    }

	@Override
	public void dispose(IoSession arg0) throws Exception {
		
	}

}