/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.comm.mina.codec;

import java.io.DataInput;

import org.apache.hadoop.io.Writable;
import org.apache.log4j.Logger;
import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

import ut.ee.newt.comm.buf.IoBufferDataInputWrapper;
import ut.ee.newt.job.BSPMessage;
import ut.ee.newt.job.JobContext;
import ut.ee.newt.misc.Packet;

public class BSPMessageDecoder extends CumulativeProtocolDecoder {

	JobContext context;
	
	private static final String DECODER_STATE_KEY = BSPMessageDecoder.class.getName() + ".STATE";
	private static Logger LOG = Logger.getLogger(BSPMessageDecoder.class);
	
	BSPMessageDecoder(JobContext context) {
		this.context = context;
	}
	
	private static class DecoderState {
		int numMsgs, current, source, superstep, available;
		int lens[];
		byte types[], status;
		Packet type;
		IoBuffer temp;
        
		DecoderState() {
			reset();
		}
		
		public void reset() {
			numMsgs = -1;
			available = -1;
			source = -1;
			status = -1;
			current = 0;
			lens = null;
			type = null;
			superstep = -1;
			if (temp != null) temp.free();
		}
	}
	
	@Override
	public boolean doDecode(IoSession session, IoBuffer in, ProtocolDecoderOutput out) throws Exception {
		DecoderState ds = (DecoderState) session.getAttribute(DECODER_STATE_KEY);
        if (ds == null) {
            ds = new DecoderState();
            session.setAttribute(DECODER_STATE_KEY, ds);
        }
        
        if (ds.type == null) {
        	ds.type = in.getEnum(Packet.class);
			LOG.debug("Received "+ds.type);
        }
        ds.available = in.remaining();
        if (ds.source < 0) {
			if (ds.available >= 8) {
				ds.source = in.getInt();
				ds.superstep = in.getInt();
				ds.available -= 8;
			} else {
	        	LOG.error("Recieved truncated message buffer!");
	        	return false;
			}
        }
        if (ds.source >= 0) {
			switch (ds.type) {
			case BUFFERED_MESSAGES:
				decodeMessages(session, in, out, ds);
				if (ds.status >= 0) return ds.status == 1;
				break;
			default:
		    	out.write(ds.type);
		    	ds.reset();
		    	return false;
			}
        }
        LOG.error("Undefined decoder state!");
        System.exit(-1);
		return false;
	}

	private void decodeMessages(IoSession session, IoBuffer in, ProtocolDecoderOutput out, DecoderState ds) throws ClassNotFoundException {
		if (ds.numMsgs < 0) {
			if (ds.available >= 4) {
				ds.numMsgs = in.getInt();
				ds.available -= 4;
			} else {
	        	LOG.error("Recieved truncated message buffer!");
			}
        }
		
		readMetadata(session, in, ds);
        
        if (ds.lens != null) {
        	DataInput din = new IoBufferDataInputWrapper(ds.temp);
        	int size = ds.lens[ds.current];
        	int oldLimit = ds.temp.limit();
        	ds.temp.limit(oldLimit+ds.available);
        	ds.temp.put(in);
        	int buffered = ds.temp.limit()-oldLimit;
        	LOG.debug("buffered "+buffered+" bytes");
        	if (ds.temp.limit() == ds.temp.capacity()) {
        		ds.temp.position(0);
	            while (ds.temp.remaining() >= size) {
	            	@SuppressWarnings("unchecked")
					Class<? extends Writable> msgType = (Class<? extends Writable>) Class.forName((String)
	            			context.getRegisteredMessageTypes().get(ds.types[ds.current]));
	            	BSPMessage msg = BSPMessage.create(din, ds.source, context.getPid(),
	            			ds.superstep, msgType);
	            	LOG.debug("Decoded: "+msg);
	            	out.write(msg);
	            	ds.available -= ds.lens[ds.current++];
	                if (ds.current == ds.numMsgs) {
		            	ds.reset();
	            		session.write(Packet.ENTERED_BARRIER);
	            		ds.status = 0;
	                	return;
	                } else {
	                	size = ds.lens[ds.current];
	                }
	            }
        	}
        	ds.temp.position(ds.temp.limit());
        	LOG.debug("expecting "+ds.lens[ds.current]+" bytes, "+ds.temp.limit()+" available");
        	ds.status = 1;
        	return;
        }
	}

	private void readMetadata(IoSession session, IoBuffer in, DecoderState ds) {
		//TODO accumulate prefix in case it's fragmented
        if (ds.numMsgs > 0 && ds.lens == null) {
        	if (ds.lens == null && ds.available >= 4 + 5*ds.numMsgs) {
        		ds.lens = new int[ds.numMsgs];
        		ds.types = new byte[ds.numMsgs];
        		int sum = 0;
    			for (int i = 0; i < ds.numMsgs; i++) {
    				ds.lens[i] = in.getInt();
    				ds.types[i] = in.get();
    				sum += ds.lens[i];
    			}
    			ds.available -= 5*ds.numMsgs;

        		//XXX do something better than allocating space for all messages at once
    			LOG.debug("Allocated "+sum+" bytes for incoming messages");
    			ds.temp = IoBuffer.allocate(sum);
    			ds.temp.limit(0);
            }
        } else if (ds.numMsgs == 0) {
        	ds.reset();
    		session.write(Packet.ENTERED_BARRIER);
        	ds.status = 0;
        	return;
        }
	}
	
}
