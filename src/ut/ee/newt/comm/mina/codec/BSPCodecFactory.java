/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.comm.mina.codec;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

import ut.ee.newt.Constants;
import ut.ee.newt.job.JobContext;

public class BSPCodecFactory implements ProtocolCodecFactory {
	
    private ProtocolEncoder[] encoder;
    private ProtocolDecoder decoder;
    
    JobContext context;

    public BSPCodecFactory(JobContext context) {
    	this.context = context;
    	int numTasks = context.getConf().getInt(Constants.NEWT_NUM_TASKS, 0);
    	
        encoder = new BSPMessageEncoder[numTasks];
        decoder = new BSPMessageDecoder(context);
		for (int i = 0; i < numTasks; i++) {
			encoder[i] = new BSPMessageEncoder(context, i+1);
		}
    }

    public ProtocolEncoder getEncoder(IoSession ioSession) throws Exception {
    	Integer id = (Integer)ioSession.getAttribute("process_id");
    	if (id == null) id = context.getPid();
    	return encoder[id];
    }

    public ProtocolDecoder getDecoder(IoSession ioSession) throws Exception {
    	return decoder;
    }
}