/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.comm.mina;

import java.io.IOException;
import java.net.InetSocketAddress;

import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.apache.hadoop.io.retry.RetryPolicy;
import org.apache.hadoop.io.retry.RetryPolicy.RetryAction;
import org.apache.log4j.Logger;
import org.apache.mina.core.future.ConnectFuture;
import org.apache.mina.core.future.ReadFuture;
import org.apache.mina.core.future.WriteFuture;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.serialization.ObjectSerializationCodecFactory;
import org.apache.mina.transport.socket.SocketSessionConfig;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;
import org.apache.mina.transport.socket.nio.NioSocketConnector;

import ut.ee.newt.Constants;
import ut.ee.newt.NEWTConfiguration;
import ut.ee.newt.comm.Communicator;
import ut.ee.newt.comm.Request;
import ut.ee.newt.comm.mina.codec.BSPCodecFactory;
import ut.ee.newt.job.JobContext;
import ut.ee.newt.misc.Packet;

public class MINACommunicator implements Communicator {

	private static final Logger LOG = Logger.getLogger(MINACommunicator.class);
	
	private static final long CONNECT_TIMEOUT = 5 * 1000L;
	private static final int IDLE_TIMEOUT = 10 * 1000;

	private NioSocketConnector connector;
	private NioSocketAcceptor acceptor;
	private NioSocketConnector[] connectors;

	private IoSession masterSession;
	private IoSession[] sessions;
	private WriteFuture[] futures;
	private ReadFuture[] rfutures;
	private ReadFuture rfuture;
	
	private int pid;
	
	
	private MINACommunicator(int pid) {
		this.pid = pid;
	}
	
	public static MINACommunicator create(JobContext context, String masterAddress, int masterPort) {
		MINACommunicator comm = new MINACommunicator(context.getPid());
		comm.setupMasterConnection(masterAddress, masterPort, context);
		return comm;
	}
	
	private class ConnectionSetupRetryHandler {
		private int fails = 0;
		private int failovers = 0;
		RetryPolicy policy;
		
		ConnectionSetupRetryHandler(JobContext context) {
			policy = NEWTConfiguration.getConnectionSetupRetryPolicy(context.getConf());
		}
		
		public void handle(Exception e) {
			try {
	    		synchronized (this) {
		    		RetryAction ra = policy.shouldRetry(e, ++fails, failovers, false);
		    		if (ra == RetryAction.FAILOVER_AND_RETRY) failovers++;
					if (ra == RetryAction.FAIL)
						throw e;
		    		wait(ra.delayMillis);
		    		LOG.info("Connection failed, retrying according to "+policy);
		    	}
			} catch (Exception err) {
				LOG.error(err.getMessage());
				err.printStackTrace();
				System.exit(-1);
			}
		}
	}

	private void setupMasterConnection(String masterHost, int masterPort, JobContext context) {
		InetSocketAddress masterAddress = new InetSocketAddress(masterHost, masterPort);
		connector = new NioSocketConnector();
		connector.getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, IDLE_TIMEOUT);
		connector.getFilterChain().addLast("codec", new ProtocolCodecFilter(new ObjectSerializationCodecFactory()));
		connector.setHandler(new IoHandlerAdapter());
		((SocketSessionConfig)connector.getSessionConfig()).setTcpNoDelay(true);
		
		ConnectionSetupRetryHandler retryHandler = new ConnectionSetupRetryHandler(context);
		while (true) try {
			ConnectFuture future = connector.connect(masterAddress);
			future.awaitUninterruptibly();
			masterSession = future.getSession();
			masterSession.getConfig().setUseReadOperation(true);
			LOG.info("Established connection to master at "+masterAddress);
			break;
		} catch (Exception e) {
	    	retryHandler.handle(e);
	    }
	}
	
	public Object requestFromMaster(Packet request) {
		masterSession.write(new Request(request, pid));
		ReadFuture future = masterSession.read();
		future.awaitUninterruptibly();
		return future.getMessage();
	}
	
	private void setupConnectionAcceptor(JobContext context) {
		acceptor = new NioSocketAcceptor();
		//acceptor.getFilterChain().addLast("logger", new LoggingFilter());
		acceptor.getFilterChain().addLast("codec", new ProtocolCodecFilter(new BSPCodecFactory(context)));
		acceptor.setHandler(new ConnectionHandler(context));
        acceptor.getSessionConfig().setReadBufferSize(2048);
        ((SocketSessionConfig)acceptor.getSessionConfig()).setTcpNoDelay(true);
        acceptor.getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, IDLE_TIMEOUT);
        final int port = Integer.parseInt(context.hosts.get(context.getPid()).toString().split(":", 2)[1]);
        InetSocketAddress addr = new InetSocketAddress(port);
        acceptor.setDefaultLocalAddress(addr);
        try {
			acceptor.bind();
		} catch (IOException e) {
			LOG.error("Failed to bind server to port: "+port+"("+e.getMessage()+")");
			System.exit(-1);
		}
	}
	
	private IoSession[] setupPeerConnections(JobContext context) {
		int numTasks = context.getConf().getInt(Constants.NEWT_NUM_TASKS, 0);
		connectors = new NioSocketConnector[numTasks];
		for (int pid = 0; pid < numTasks; pid++) {
			if (pid != context.getPid()) {
				connectors[pid] = new NioSocketConnector();
				//connectors[pid].getFilterChain().addLast("logger", new LoggingFilter());
				connectors[pid].getFilterChain().addLast("codec", new ProtocolCodecFilter(new BSPCodecFactory(context)));
				connectors[pid].setConnectTimeoutMillis(CONNECT_TIMEOUT);
				connectors[pid].getSessionConfig().setIdleTime(IdleStatus.BOTH_IDLE, IDLE_TIMEOUT);
				connectors[pid].setHandler(new ProcessConnector(context, pid));
				((SocketSessionConfig)connectors[pid].getSessionConfig()).setTcpNoDelay(true);
			}
		}
		LOG.info("Trying to establish connections to peers...");
		IoSession[] sessions = new IoSession[numTasks];
		for (int pid = 0; pid < numTasks; pid++) {
			if (pid != context.getPid()) {
				ConnectionSetupRetryHandler retryHandler = new ConnectionSetupRetryHandler(context);
				while (true) try {
					String[] addr = context.hosts.get(pid).toString().split(":", 2);
					int port = Integer.parseInt(addr[1]);
			        ConnectFuture cfuture = connectors[pid].connect(new InetSocketAddress(addr[0], port));
			        LOG.info("Connecting to peer at "+addr[0]+":"+port);
			        cfuture.awaitUninterruptibly();
			        sessions[pid] = cfuture.getSession();
			        LOG.info("Established connection with peer at "+addr[0]+":"+port);
			    	break;
			    } catch (Exception e) {
			    	retryHandler.handle(e);
			    }
			}
		}
		return sessions;
	}

	@Override
	public void dispose() {
		try {
			acceptor.dispose();
		} catch (Exception ex) {
			LOG.error(ex.getMessage());
		}
		try {
			connector.dispose();
		} catch (Exception ex) {
			LOG.error(ex.getMessage());
		}
		for (NioSocketConnector c : connectors) {
			try {
				if (c != null) c.dispose();
			} catch (Exception ex) {
				LOG.error(ex.getMessage());
			}
		}
	}


	@Override
	public void init(JobContext context) {
		setupConnectionAcceptor(context);
		sessions = setupPeerConnections(context);
		futures = new WriteFuture[context.getNumTasks()];
		rfutures = new ReadFuture[context.getNumTasks()];
	}

	@Override
	public void reinit(JobContext context) {
		context.hosts = (DualHashBidiMap) requestFromMaster(Packet.GET_HOSTS);
		sessions = setupPeerConnections(context);
	}

	@Override
	public void sendBufferedMessagesAsync(JobContext context, int pid) {
		sessions[pid].getConfig().setUseReadOperation(true);
		futures[pid] = sessions[pid].write(Packet.BUFFERED_MESSAGES);
	}

	@Override
	public void waitUntilBufferedMessagesSent(JobContext context, int pid) {
		futures[pid].awaitUninterruptibly();
	}

	@Override
	public Object requestFromMasterSync(Request request) {
		requestFromMasterAsync(request);
		
		return waitUntilAnswerFromMasterAsync();
	}
	
	@Override
	public void requestFromMasterAsync(Request request) {
		WriteFuture wfut = masterSession.write(request);
		wfut.awaitUninterruptibly();
		//LOG.debug("Requested from master: "+request);
	}

	@Override
	public Object waitUntilAnswerFromMasterAsync() {
		rfuture = masterSession.read();
		Object answer = null;
		//XXX why is read from master returning null?
		while (answer == null) {
			rfuture.awaitUninterruptibly();
			answer = rfuture.getMessage();
		}
		//LOG.debug("Got: "+answer);
		return answer;
	}

	@Override
	public boolean isConnectedToProcess(int pid) {
		return sessions[pid].isConnected();
	}

	@Override
	public void readFromProcessAsync(int pid) {
		rfutures[pid] = sessions[pid].read();
	}

	@Override
	public Object waitUntilReadFromProcessAsync(int pid) {
		rfutures[pid].awaitUninterruptibly();
		Object reply = rfutures[pid].getMessage();
		sessions[pid].getConfig().setUseReadOperation(false);
		return reply;
	}

}
