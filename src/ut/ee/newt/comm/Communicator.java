/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.comm;

import ut.ee.newt.job.JobContext;

public interface Communicator {

	void init(JobContext context);

	void reinit(JobContext context);

	void sendBufferedMessagesAsync(JobContext context, int pid);

	void waitUntilBufferedMessagesSent(JobContext context, int pid);
	
	Object requestFromMasterSync(Request request);

	void requestFromMasterAsync(Request request);

	Object waitUntilAnswerFromMasterAsync();

	boolean isConnectedToProcess(int pid);

	void readFromProcessAsync(int pid);

	Object waitUntilReadFromProcessAsync(int pid);

	void dispose();

}
