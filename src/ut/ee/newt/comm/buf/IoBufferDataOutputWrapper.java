/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.comm.buf;

import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;

import org.apache.mina.core.buffer.IoBuffer;

public class IoBufferDataOutputWrapper implements DataOutput {

	IoBuffer buffer;
	
	private DataOutputStream dos;
	
	public IoBufferDataOutputWrapper(IoBuffer buffer) {
		this.buffer = buffer;
		this.dos = new DataOutputStream(buffer.asOutputStream());
		
	}
	
	@Override
	public void write(int b) throws IOException {
		dos.write(b);
	}

	@Override
	public void write(byte[] b) throws IOException {
		dos.write(b);
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException {
		dos.write(b, off, len);
	}

	@Override
	public void writeBoolean(boolean v) throws IOException {
		dos.writeBoolean(v);
	}

	@Override
	public void writeByte(int v) throws IOException {
		dos.writeByte(v);
	}

	@Override
	public void writeBytes(String s) throws IOException {
		dos.writeBytes(s);
	}

	@Override
	public void writeChar(int v) throws IOException {
		dos.writeChar(v);
	}

	@Override
	public void writeChars(String s) throws IOException {
		dos.writeChars(s);
	}

	@Override
	public void writeDouble(double v) throws IOException {
		dos.writeDouble(v);
	}

	@Override
	public void writeFloat(float v) throws IOException {
		dos.writeFloat(v);
	}

	@Override
	public void writeInt(int v) throws IOException {
		dos.writeInt(v);
	}

	@Override
	public void writeLong(long v) throws IOException {
		dos.writeLong(v);
	}

	@Override
	public void writeShort(int v) throws IOException {
		dos.writeShort(v);
	}

	@Override
	public void writeUTF(String s) throws IOException {
		dos.writeUTF(s);
	}

}
