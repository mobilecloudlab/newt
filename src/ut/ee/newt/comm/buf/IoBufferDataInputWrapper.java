/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.comm.buf;

import java.io.BufferedReader;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.mina.core.buffer.IoBuffer;

public class IoBufferDataInputWrapper implements DataInput {
	
	IoBuffer buffer;

	private DataInputStream dis;
	private BufferedReader br;
	
	public IoBufferDataInputWrapper(IoBuffer buffer) {
		this.buffer = buffer;
		dis = new DataInputStream(buffer.asInputStream());
		br = new BufferedReader(new InputStreamReader(dis));
	}

	@Override
	public boolean readBoolean() throws IOException {
		return dis.readBoolean();
	}

	@Override
	public byte readByte() throws IOException {
		return dis.readByte();
	}

	@Override
	public char readChar() throws IOException {
		return dis.readChar();
	}

	@Override
	public double readDouble() throws IOException {
		return dis.readDouble();
	}

	@Override
	public float readFloat() throws IOException {
		return dis.readFloat();
	}

	@Override
	public void readFully(byte[] b) throws IOException {
		dis.readFully(b);
	}

	@Override
	public void readFully(byte[] b, int off, int len) throws IOException {
		dis.readFully(b, off, len);
	}

	@Override
	public int readInt() throws IOException {
		return dis.readInt();
	}

	@Override
	public String readLine() throws IOException {
		return br.readLine();
	}

	@Override
	public long readLong() throws IOException {
		return dis.readLong();
	}

	@Override
	public short readShort() throws IOException {
		return dis.readShort();
	}

	@Override
	public String readUTF() throws IOException {
		return dis.readUTF();
	}

	@Override
	public int readUnsignedByte() throws IOException {
		return dis.readUnsignedByte();
	}

	@Override
	public int readUnsignedShort() throws IOException {
		return dis.readUnsignedShort();
	}

	@Override
	public int skipBytes(int n) throws IOException {
		return dis.skipBytes(n);
	}

}
