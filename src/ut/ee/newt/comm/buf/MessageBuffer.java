/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.comm.buf;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.mina.core.buffer.IoBuffer;

import ut.ee.newt.job.BSPMessage;
import ut.ee.newt.job.JobContext;

public class MessageBuffer {
	
	private static final Logger LOG = Logger.getLogger(MessageBuffer.class);

	IoBuffer buf;
	
	List<Integer> lens;
	List<String> types;
	
	private DataInput in;
	private DataOutput out;
	
	JobContext context;
	
	public MessageBuffer(JobContext context) {
		this.context = context;
		
		buf = IoBuffer.allocate(2048);
		buf.setAutoExpand(true);
		
		lens = new ArrayList<Integer>();
		types = new ArrayList<String>();
		
		in = new IoBufferDataInputWrapper(buf);
		out = new IoBufferDataOutputWrapper(buf);
	}

	public Object getPrefix() {
		IoBuffer prefix = IoBuffer.allocate(4 + 5*lens.size());
		prefix.putInt(lens.size());
		for (int i = 0; i < lens.size(); i++) {
			prefix.putInt(lens.get(i));
			try {
				prefix.put((Byte)context.getRegisteredMessageTypes().getKey(types.get(i)));
			} catch (NullPointerException ex) {
				LOG.error("Unregistered message type being sent: '"+types.get(i)+"' of size "+lens.get(i));
				System.exit(-1);
			}
		}
		prefix.flip();
		return prefix;
	}

	public boolean isEmpty() {
		int bytes = 0;
		for (int b : lens)
			bytes += b;
		return bytes == 0;
	}

	public IoBuffer get() {
		buf.flip();
		return buf;
	}

	public void clear() {
		buf.clear();
		lens.clear();
		types.clear();
	}

	public void write(BSPMessage msg) {
		int pos = buf.position();
		try {
			LOG.debug("Encoding: "+msg);
			msg.write(out);
		} catch (IOException e) {
			LOG.error(e.getMessage());
		}
		lens.add(buf.position()-pos);
		types.add(msg.getMessageType().getName());
	}
	
	DataInput getInputHandle() {
		return in;
	}
	
	DataOutput getOutputHandle() {
		return out;
	}
}
