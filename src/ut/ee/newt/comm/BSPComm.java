/**
* Licensed to the Mobile Cloud Lab (University of Tartu),
* under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

package ut.ee.newt.comm;

import java.util.ArrayDeque;

import org.apache.hadoop.io.Writable;
import org.apache.log4j.Logger;

import ut.ee.newt.job.BSPMessage;
import ut.ee.newt.job.JobContext;
import ut.ee.newt.misc.Packet;

public class BSPComm {
	
	private static final Logger LOG = Logger.getLogger(BSPComm.class);
	
	private Request exit_barrier = new Request(Packet.EXIT_BARRIER,-1);
	
	private Communicator comm;
	
	private JobContext context;
	
	boolean sendMask[];
	
	int procIds[];
	
	public BSPComm(JobContext context, Communicator comm) {
		this.context = context;
		this.comm = comm;
		
		sendMask = new boolean[context.getNumTasks()];
		
		procIds = new int[context.getNumTasks()-1];
		int j = 0;
		for (int i = 0; i < context.getNumTasks(); i++) {
			if (i != pid())
				procIds[j++] = i;
		}

		LOG.debug("Initializing communicator...");
		comm.init(context);
	}
	
	public void restartPeerSessions() {
		comm.reinit(context);
		resetSendMask();
	}
	
	public void clearUserQueues() {
		for (int pid : procIds) {
			context.userQueue[pid].clear();
		}
	}
	
	public int sync() {
		int status = 0;
		//clearUserQueues();
		
		ArrayDeque<BSPMessage> own = context.sendQueue[pid()];
		for (BSPMessage msg : own) {
			context.userQueue[pid()].addFirst(msg);
		}
		own.clear();
		for (int pid : procIds) {
			if (context.sendQueue[pid].size() > 0) {
				sendMask[pid] = true;
		        context.sendBuf[pid].clear();
		        ArrayDeque<BSPMessage> q = context.sendQueue[pid];
		        while (!q.isEmpty()) {
		        	BSPMessage m = q.poll();
		        	context.sendBuf[pid].write(m);
		        }
				comm.sendBufferedMessagesAsync(context, pid);
			}
		}
		
		for (int pid : procIds) {
			try {
				if (sendMask[pid]) {
					comm.waitUntilBufferedMessagesSent(context, pid);
					context.sendQueue[pid].clear();
				}
			} catch (Exception ex) {
				LOG.error(ex.getMessage());
			}
		}
		
		status = barrier();
		
		return status;
	}
	
	private int barrier() {
		int status = 0;
		
		sendBufferedMessages();
		
		exit_barrier.source = context.superstep;
		Packet barrierExitStatus = (Packet) comm.requestFromMasterSync(exit_barrier);
		
		switch (barrierExitStatus) {
		case CHECKPOINT_NEEDED:
			status = -1;
		case EXITED_BARRIER:
			context.superstep++;
			//populate pid-specific queues, synchronized to avoid
			//concurrent modification of recvQueue by the communicator
			synchronized (context.recvQueue) {
				for (BSPMessage msg : context.recvQueue) {
					if (msg.superstep == context.superstep-1) {
						context.userQueue[msg.source].push(msg);
					}
				}
				for (int pid : procIds) {
					context.recvQueue.removeAll(context.userQueue[pid]);
				}
				resetSendMask();
			}
			break;
		case RECOVERY_NEEDED:
			status = (Integer) comm.waitUntilAnswerFromMasterAsync();
			break;
		default:
			LOG.error("Barrier synchronization fault (unexpected "+barrierExitStatus+")");
		}
		return status;
	}
	
	private void sendBufferedMessages() {
		for (int pid : procIds) {
			if (sendMask[pid]) {
				if (!comm.isConnectedToProcess(pid)) {
					LOG.warn("Session with process "+pid+" is not open");
					sendMask[pid] = false;
					continue;
				}
				comm.readFromProcessAsync(pid);
			}
		}
		
		for (int pid : procIds) {
			if (sendMask[pid]) {
				try {
					Packet reply = (Packet) comm.waitUntilReadFromProcessAsync(pid);
					if (reply != Packet.ENTERED_BARRIER)
						LOG.error("Barrier synchronization fault! (got "+reply
								+", expected "+Packet.ENTERED_BARRIER+")");
				} catch (Exception ex) {
					LOG.error(ex.getMessage());
				}
			}
		}
	}

	private void resetSendMask() {
		for (int pid : procIds) {
			sendMask[pid] = false;
		}
	}
	
	public void send(int pid, Writable message) {
		if (pid < 0 || pid > procIds.length) {
			LOG.error("Process with Id "+pid+" does not exist");
		} else {
			context.sendQueue[pid].push(new BSPMessage(pid(), pid, context.getSuperstep(), message));
		}
	}
	
	public Writable move() {
		Writable p = null;
		if (!context.userQueue[pid()].isEmpty()) {
			p = context.userQueue[pid()].poll().payload;
			return p;
		}
		for (int pid : procIds) {
			ArrayDeque<BSPMessage> rq = context.userQueue[pid];
			if (!rq.isEmpty()) {
				p = rq.poll().payload;
				break;
			}
		}
		if (p == null) {
			LOG.error("The receive queue is empty!");
			return p;
		} else {
			return p;
		}
	}
	
	public Writable move(int pid) {
		ArrayDeque<BSPMessage> rq = context.userQueue[pid];
		if (rq.isEmpty()) {
			LOG.error("The receive queue-"+pid+" is empty!");
			return null;
		} else {
			return rq.poll().payload;
		}
	}
	
	public BSPMessage peek() {
		BSPMessage p = null;
		p = context.userQueue[pid()].peek();
		if (p != null) return p;
		for (int pid : procIds) {
			p = context.userQueue[pid].peek();
			if (p != null) break;
		}
		return p;
	}
	
	public BSPMessage peek(int pid) {
		return context.userQueue[pid].peek();
	}
	
	public final int pid() {
		return context.getPid();
	}
	
	public final int nprocs() {
		return context.getNumTasks();
	}

	public int superstep() {
		return context.superstep;
	}
}
